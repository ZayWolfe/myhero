//the scenes of the game
ax.direct.saveScene("gameplay", function (){
      
    axr.audio.reset();
    
    //make and store the interface objects
    world.regions[mh.currentRegion].make();
	mh.aScreen = mh.mAScreen();
	mh.eScreen = mh.mEScreen();
	mh.cScreen = mh.mCScreen();
    //restor info if coming back from another scene
    mh.transition();
    //hero objects 
    makeTarget();
	makeHero();
    //game icons
    var saveIcon = new ax.Actor({
        type: "icon",
        width: 28,
        height: 28,
        pos:[150,17],
        stamp: axr.images.saveIcon,
        fixed: true,
        layer: 4
    });
    saveIcon.leftUp = function(){
        axe.mouse.leftUp = false;
        mh.mSaveDialog();
    }
    var charIcon = new ax.Actor({
        type: "icon",
        width: 28,
        height: 28,
        pos:[182,17],
        stamp: axr.images.charIcon,
        fixed: true,
        layer: 4
    });
    charIcon.leftUp = function(){
        axe.mouse.leftUp = false;
        mh.transition(true);
        ax.direct.playScene("character");
    }
	
	ax.gameLoop = mh.loop;
    //audio
    
	axr.audio.village.play();    
    mh.inCity = false;
});

//temporary city screen
ax.direct.saveScene("city",function () {
    mh.inCity = true;
    var wind = null;
    
    axr.audio.reset();
    axr.audio.town.play();//temporary music
    ax.setBack("#EEE685");
    
    //grab the city object and make it found if it's not,
    //for fast travel
    var city = world.regions[mh.currentRegion].returnLocation(mh.currentLocation);
    if(!city.found){
        city.found = true;
    }
    mh.saveGameData();
    
    var mkB = (function (){
        var pos = 0;
        var place = "City";
        
        return function (name, func) {
            var button = new ax.Actor({
                pos: [-180+pos,-200],
                height:32,
                width:106,
                layer:2,
                stamp: axr.images.citybutton
            });
            pos += 120;
            button.text.txt = name;
            button.text.width = 55;
            button.text.height = 20;
            
            button.gameScript = function(){
                if(button.mhover || (place === name)){
                    button.stamp = axr.images.citybuttonlit;
                } else if(!button.mhover || (place != name)){
                    button.stamp = axr.images.citybutton;
                }
            };
            button.leftUp = function (){
                if(wind)
                    wind.killall();
                place = name;
                if(func){
                    wind = func();
                }
            };
            
        };
    })();
    
    //the normal city interface setup
    var back = new ax.Actor({
        height:500,
        width:700,
        layer:1,
        stamp: axr.images.cityBack
    });
    var backimg = new ax.Actor({
        height:500,
        width:700,
        layer:0,
        stamp: axr.images[city.background]
    });
    //the buttons
    mkB("City");
    mkB("Crafting",mh.mCityCraft);
    mkB("Shop",mh.mCityShop);
    
    //city/hero information actor
    var infoActor = new ax.Actor({
        pos:[290,-50],
        height:225,
        width: 110,
        layer:3
    });
    function updateCityInfo() {
        var info = "City: " + city.name + "         Pop: " + city.pop +
        "         Fans: " + city.fans + "         Rest: " + city.restCost +
        "g" + "         Eco: " + city.economy + "                  " +
        "                 Hero: " + mh.hero.name + "      Health: " +
        Math.round(mh.hero.hp[0]);
        
        infoActor.text.txt = info;
    }
    updateCityInfo();
    
    
    //the button to return to the main game
    var leaveActor = new ax.Actor({
        pos:[300,140],
        height:30,
        width: 100,
        layer:3
    });
    leaveActor.gameScript = function(){
        if(leaveActor.mhover){
            leaveActor.alpha = 1.0;
        }else {
            leaveActor.alpha = 0.5;
        }
    };
    leaveActor.text.color = "#ffffff";
    leaveActor.text.size = 18;
    leaveActor.text.weight = "bold";
    leaveActor.text.txt = "Return";
    leaveActor.leftUp = function(){
        axe.mouse.leftUp = false;
        mh.saveGameData();
        
        ax.direct.playScene("gameplay");
    };
    //button to rest
    var restActor = new ax.Actor({
        pos:[300,105],
        height:30,
        width: 100,
        layer:3
    });
    restActor.gameScript = function(){
        if(restActor.mhover){
            restActor.alpha = 1.0;
        }else {
            restActor.alpha = 0.5;
        }
    };
    restActor.text.color = "#ffffff";
    restActor.text.size = 18;
    restActor.text.weight = "bold";
    restActor.text.txt = "Rest";
    restActor.leftUp = function(){
        axe.mouse.leftUp = false;
        
        if(mh.hero.gold >= city.restCost){
            if(confirm("Do you want to rest at the Inn? "+city.restCost+"g")){
                mh.hero.hp[0] = mh.hero.hp[1];
                mh.hero.gold -= city.restCost;
            }
        }else {
            alert("Not enough gold.");
        }
        updateCityInfo();
    };
});

/*
 *The character creation will be an extremely important part
 *of the game. For now it merely creates a base hero and moves
 *along. in the future there must be an extensive creation system.
 */
ax.direct.saveScene("charCreation", function() {
    axr.audio.reset();
    //set base location
    mh.currentLocation = "Ys";
    mh.currentRegion = "Durra";
    
    //make base character
    mh.generateBaseHero();
    
    ax.direct.playScene("gameplay");
});

//character info screen and skill point allocation
ax.direct.saveScene("character", function(){

    ax.setBack("#393939");
    
    
    mh.cScreen = mh.mCScreen([105,110]);
    var exp = new ax.Actor({
        width:100,
        height:20,
        pos: [-280,100]
    });
    exp.text.color = "#ffffff";
    exp.text.txt = "Exp: "+mh.hero.exp;
    
    //the button to return to the main game
    var leaveActor = new ax.Actor({
        pos:[300,200],
        height:30,
        width: 100,
        layer:3
    });
    leaveActor.gameScript = function(){
        if(leaveActor.mhover){
            leaveActor.alpha = 1.0;
        }else {
            leaveActor.alpha = 0.5;
        }
    };
    leaveActor.text.color = "#ffffff";
    leaveActor.text.size = 18;
    leaveActor.text.weight = "bold";
    leaveActor.text.txt = "Return";
    leaveActor.leftUp = function(){
        axe.mouse.leftUp = false;
        mh.saveGameData();
        
        ax.direct.playScene("gameplay");
    };
    
});

//the loading screen, sets some variables and makes sure all
//resources to the game have loaded.
ax.direct.saveScene("loading", function() {
    axr.audio.setVolume(0.1);
    ax.setBack("#000000");

    
    ax.gameLoop = function(){
        if(axr.isLoaded()){
            //check to see if any save data is available, otherwise creat
            //new character
            if(!mh.loadGameData()){
                
                ax.direct.playScene("charCreation");
            }else {
                ax.direct.playScene("gameplay");
            }
        }
    };
    
});