// axis drawing api
var axd = {};
//draws a circle with an optional border
//and optional color. Use center=true if used to
//draw an actor
axd.cir = function (center, x, y, rad, fill, border){
    if(!center){
        x+=ax.scrollx;
		y+=ax.scrolly;
    }
    var c = ax.ctx;
    c.save();
    c.beginPath();
    c.arc(x,y,rad,0,Math.PI*2,false);
    c.closePath();
    if(fill){
		//add something for transparency
		c.fillStyle = "rgb("+fill[0]+","+fill[1]+","+fill[2]+")";
		c.fill();
    }
    if(border){
		c.lineWidth = border[0];
		c.strokeStyle = "rgb("+border[1]+","+border[2]+","+border[3]+")";
		c.stroke();
    }
    c.restore();
    
};

axd.poly = function(poly, x, y){
    var ofx = ax.scrollx+x;
    var ofy = ax.scrolly+y;
    var c = ax.ctx;
    
    var i = 1;
    var l = poly.length;
    c.save();
    c.strokeStyle = "rgb(250,0,0)";
    c.beginPath();
    c.moveTo(poly[0][0]+ofx,poly[0][1]+ofy);
    
    while(i < l){
        c.lineTo(poly[i][0]+ofx,poly[i][1]+ofy);
        i++;
    }
    
    c.lineTo(poly[0][0]+ofx,poly[0][1]+ofy);
    c.closePath();
    c.stroke();
    c.restore();
};

axd.rect = function (center,x,y,width,height,fill,border){
    if(!center){
		x+=ax.scrollx;
		y+=ax.scrolly;
    }
    var c = ax.ctx;
    c.save();
    if(fill){
		//add something for transparency
		c.fillStyle = "rgb("+fill[0]+","+fill[1]+","+fill[2]+")";
		c.fillRect(x-width/2,y-height/2,width,height);
    }
    if(border){
		c.lineWidth = border[0];
		c.strokeStyle = "rgb("+border[1]+","+border[2]+","+border[3]+")";
		c.strokeRect(x-width/2,y-height/2,width,height);
    }
    c.restore();
};

axd.image = function (center, image, x, y){
    if(!center){
		x+=ax.scrollx;
		y+=ax.scrolly;
    }
    var c = ax.ctx;
    /*try{
        c.drawImage(image,x-image.width/2,y-image.height/2);
    }catch(e){
        throw -image.width/2+","+y-image.height/2+" "+e;
    }*/
	c.drawImage(image,x-image.width/2,y-image.height/2);

};

axd.sprite = function (center,image,cx,cy,cwidth,cheight,dx,dy,dwidth,dheight){
    if(!center){
		dx+=ax.scrollx;
		dy+=ax.scrolly;
    }
    var c = ax.ctx;

    c.save();
    c.drawImage(
        image,
        cx,cy,cwidth,cheight,
        dx-cwidth/2,dy-cheight/2,dwidth,dheight
        );
    c.restore();
};

axd.clr = function (){
	/*var c = ax.ctx;
    c.save();
    c.setTransform(1,0,0,1,0,0);
    c.clearRect(0,0,ax.width,ax.height);
    c.restore();*/
	ax.canvas.width = ax.canvas.width;
    
};
