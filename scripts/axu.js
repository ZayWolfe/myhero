// utility functions of axis engine

//find angle betweent two points in c: angle = atan2(y2 - y1, x2 - x1) * 180 / PI;
var axu = {};
axu.sign = function(num){
    if (num < 0) return -1;
	if (num > 0) return 1;
	return 0;
};
axu.randBet = function(from, to){
	return Math.floor(Math.random() * (to-from+1)+from);
};
axu.randTrue = function(base){
    base = Math.floor(base);
    var i = 0;
    var tnum = 0;
    while(i <= base){
        tnum += axu.randBet(0,1);
        i++;
    }
    if(base-tnum > base/2)
        return true;
    else
        return false;
};
axu.probability = function (base){
    base = Math.floor(base);
    if(base > 100)
        base = 100;
	if(base < 0)
		base = 0;
	
    var num = axu.randBet(0,100);
    if(num <= base)
        return true;
    else
        return false;
};
//keeps the number given positive or stops at 0
axu.keepPositive = function(x){
    if(x < 0)
        return 0;
    else
        return x;
};

axu.isOnScreen = function(obj){
	//check if the object is on screen
	var onScreen = true;
	if(obj.fixed){
		var scrollx = 0;
		var scrolly = 0;
	} else {
		var scrollx = ax.scrollx;
		var scrolly = ax.scrolly;
	}
	
	if(obj.pos[0]+obj.width/2+scrollx < 0){
		onScreen = false;
	}
	if(obj.pos[0]-obj.width/2+scrollx > ax.width){
		onScreen = false;
	}
	if(obj.pos[1]+obj.height/2+scrolly < 0){
		onScreen = false;
	}
	if(obj.pos[1]-obj.height/2+scrolly > ax.height){
		onScreen = false;
	}
	if(!obj.onScreen && !onScreen){//do the events or return if off screen
		return onScreen;
	} else if(obj.onScreen && !onScreen){
		obj.onScreen = false;
		if(obj.onScreenExit)obj.onScreenExit(); //run event
		return onScreen;
	} else if(!obj.onScreen && onScreen){
		obj.onScreen = true;
		if(obj.onScreenEnter)obj.onScreenEnter(); //run event
	}
	
	return onScreen;
};
axu.makeTxtImg = function (txt){
	var img = document.createElement("canvas");
	var ctx = img.getContext("2d");
	var font = txt.weight + " " + txt.size + "pt " +txt.font;
	var textarray = axu.wordWrap(txt.txt, txt.width, font);
	
	img.width = txt.width;
	img.height = txt.height;
	if(ax.debug){
		ctx.fillStyle = "red";
		ctx.fillRect(0,0,img.width,img.height);/**/
	}
	ctx.fillStyle = txt.color;
	ctx.font = font;
	
	for(var i = 0,l = textarray.length; i<l; i++){
		ctx.fillText(textarray[i], 0, txt.size*1.2+txt.spacing*i);  
	}
	return img;
};

axu.isMouseOver = function (obj){
	var ms;
	if(!obj.fixed){
		ms = [axe.mouse.pos[0]-ax.scrollx,axe.mouse.pos[1]-ax.scrolly];
	} else {
		ms = [axe.mouse.pos[0],axe.mouse.pos[1]];
	}
	
	if((ms[0] > obj.pos[0]-obj.width/2 && ms[0] < obj.pos[0]+obj.width/2) &&
	(ms[1] > obj.pos[1]-obj.height/2 && ms[1] < obj.pos[1]+obj.height/2)){
		obj.mhover = true;
		//do mouse events
		if(axe.mouse.leftDown){
			if(obj.leftDown)obj.leftDown();
		}
		if(axe.mouse.midDown){
			if(obj.midDown)obj.midDown();
		}
		if(axe.mouse.rightDown){
			if(obj.rightDown)obj.rightDown();
		}
		if(axe.mouse.leftUp){
			if(obj.leftUp)obj.leftUp();
		}
		if(axe.mouse.midUp){
			if(obj.midUp)obj.midUp();
		}
		if(axe.mouse.rightUp){
			if(obj.rightUp)obj.rightUp();
		}
	}else{
		obj.mhover = false;
	}
};

axu.wordWrap = function (text, width, font){
	ax.ctx.save();
	ax.ctx.font = font;
    var buffer = [];
    var txtRaw = text.split(" ");
	var lineraw = "";
	var line = "";
	
    for(var i = 0,l = txtRaw.length; i < l; i++){
		line = lineraw + txtRaw[i];
		
		if(ax.ctx.measureText(line).width >= width){
			
			buffer.push(lineraw);
			lineraw = "";
			line = "";
		}
		lineraw = lineraw + txtRaw[i] + " ";
		if(i+1 === l){
			buffer.push(line);
		}
    }
	ax.ctx.restore();
	return buffer;
};

axu.largestOfArray = function (array){
	return Math.max.apply( Math, array );
};

//give it two angles and it returns true if they are facing
//within the degree spacing you give it
axu.isfacing = function (act1,act2,area){//thanks to John Mcdonald
	var toface = axu.direction(act1.pos,act2.pos);
	var left = toface - area;
    var right = toface + area;

    // Wrap the left and right angles
    if(left < 0){ left += 360; }
    if(right >= 360){ right -= 360; }

    // Do 2 checks:
    // - Where the left is smaller than the right, we should be between both of them.
    // - Where the either the left or right has wrapped around 360 (left > right),
    //   we only need to be facing such that we are either greater than the
    //   left OR less than the right
    if ((left < right && act1.angle > left && act1.angle < right) ||
        (left > right && (act1.angle > left || act1.angle < right))){
        return true;
    }
    return false;
};

//finds the angle pointing from one vector to another
axu.direction = function(v1,v2,inRadians){
    var radians = Math.atan2(v2[1]-v1[1],v2[0]-v1[0]);
    
    return (inRadians) ? radians: axu.rad2degrees(radians);
};
axu.rad2degrees = function(radians){
    radians *= (180/Math.PI);
    if(radians< 0 )
        return radians+360;
    else
        return radians;
};
axu.findSideOf = function(v1,v2){
   return [axu.sign(v1[0]-v2[0]),axu.sign(v1[1]-v2[1])]; 
};

axu.findBounce = function(ball,line){
	var offset = line - 90;
	result = ball - offset;
	result = 90  - result;
	result = 90 + result;
	return result + offset;
};

axu.getLength = function (xy) {
	return Math.sqrt(xy[0]*xy[0]+xy[1]*xy[1]);
};
axu.getAngle = function (xy,inRadians){
	var angle = Math.atan2(xy[0],xy[1]);
	return (inRadians) ? angle: axu.rad2degrees(angle);
};
axu.getNormal = function(xy){
	var length = axu.getLength(xy);
	return (length === 0)? 0 : [xy[0]/length,xy[1]/length];
};

axu.distance = function (v1, v2){
    var disx = v2[0] - v1[0];
    var disy = v2[1] - v1[1];
    
    return Math.abs(Math.sqrt(disx*disx+disy*disy));
};

//dot product
axu.dot = function(v1,v2){
    return v1[0]*v2[0]+v1[1]*v2[1];
};

axu.padNum = function (num, base){
    var i = (''+num).length;
    var l = (''+base).length;
    if(l === 1){
        return '0'+num;
    }
    
    while (i < l){
        num = '0'+num;
        i++;
    }
    return num;
};

//checks if a point give is within the poly given
axu.inpoly = function(poly, x, y){
    var i = 0, j, xp = [], yp = [], c = false, npol = poly.length;
    while(i < npol){
        xp.push(poly[i][0]);
        yp.push(poly[i][1]);
        i++;
    }
    i = 0;
    j = npol-1;
    while (i < npol) {
        if ((((yp[i] <= y) && (y < yp[j])) ||
        ((yp[j] <= y) && (y < yp[i]))) &&
        (x < (xp[j] - xp[i]) * (y - yp[i]) / (yp[j] - yp[i]) + xp[i]))
        c =!c;
        
        j = i++;
    }
    return c;
};
axu.polyCentoid = function(shape){
   var i = 0,x = 0,y = 0;
   for(var l = shape.length; i<l; i++){
        x += shape[i][0];
        y += shape[i][1];
    }
    return [x/(i+1),y/(i+1)];
};
