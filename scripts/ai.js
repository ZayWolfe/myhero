// The ai for the hero

axi.addBehavior({
    name: "hero",
    start: function (act) {
        if(act.type === "hero"){
            return true;
        }else {
            return false;
        }
    }
});

axi.addBehavior({
    place: axi.b.hero,
    name: "move",
    start: function (act){
        if(axu.distance(act.pos, mh.target.pos) > 30+mh.hero.wonder){
            act.speed = mh.hero.spd/100;
            act.angle = axu.direction(act.pos, mh.target.pos) +
                axu.randBet(-20, 20);
        }else {
            act.speed = mh.hero.spd/100;
            act.angle += axu.randBet(-8, 8);
        }
        if(!mh.hero.move){
            act.speed = 0;
        }
        
    }
});

axi.addBehavior({
    place:axi.b.hero,
    name: "events",
    start: (function(){
        var cnt = 0;
        var end =  null;
        return function(act){
            if(!end)
                end = axu.randBet(8*ax.fps, 60*ax.fps);
            
            if(cnt > end ){
                if(mh.hero.move || axu.probability(8)){
                    startLightEvent();
                }
                
                end = axu.randBet(5*ax.fps, 40*ax.fps);
                cnt = 0;
            }
            cnt++;
        };
    })()
});
