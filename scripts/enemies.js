//monster Information

mh.enemies = {};

mh.enemies.rat = {
    name: "Rat",
    hp:18,
    str:6,
    def:3,
    agi:4,
    luck:3,
    lvl:1
};

mh.enemies.beatle = {
    name: "Beatle",
    hp:18,
    str:6,
    def:3,
    agi:2,
    luck:3,
    lvl:1
};

mh.enemies.manfox = {
    name: "Manfox",
    hp: 22,
    str: 3,
    def: 4,
    agi:4,
    luck:6,
    lvl:2
};

mh.enemies.spider = {
    name: "Spider",
    hp: 15,
    str: 7,
    def: 6,
    agi:2,
    luck: 4,
    lvl: 2
}

mh.enemies.lvlPicker = function (lvl){
    var level = mh.hero.lvl;
    var reach = Math.abs(lvl - level);
    var end = 3;
    
     if( reach < 3 ){
        return true;
     }else {
        return false;
     }
}

var pickEnemy = function(){
    var list = [];
    for(iterator in mh.enemies){
        if(mh.enemies.lvlPicker(mh.enemies[iterator].lvl)){
            if(mh.enemies[iterator].lvl){
                list.push(mh.enemies[iterator]);
            }
        }
    }
    return list[axu.randBet(0,list.length-1)];
}