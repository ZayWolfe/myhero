//axis resource loading 
var axr = {};
axr.tiles = {};
axr.audio = {};
axr.video = {};
axr.images = {};
axr.loadfails = 0;
axr.loadstalls = 0;

axr.audio.setVolume = function (num){
    var i;
    for(i in axr.audio){
        if(axr.audio[i].axisResource === true)
            axr.audio[i].volume = num;
    }
};

axr.audio.reset = function (){
    var i;
    for(i in axr.audio){
        if(axr.audio[i].axisResource === true){
            if(axr.audio[i].stop){
              axr.audio[i].stop();
            }else{
              axr.audio[i].pause();
            }
            axr.audio[i].currentTime = 0;
        }
    }
};

axr.isLoaded = function (){
    axr.perLoaded = [0,0];
    var src; 
    var allLoaded = true;
    
    for(var i in axr.images){
        //select only the created resources
        if(axr.images[i].axisResource === true){
            axr.perLoaded[1]++;
            
            if(axr.images[i].width < 1 || !axr.images[i].ready){
                allLoaded = false;
                
                //if the amount of loadfails exceeds 100
                //reload those elements
                if(axr.loadfails === 100){
                    var nimg = new Image();
                    nimg.addEventListener("load", function(){
                        this.ready = true;
                    });
                    nimg.axisResource = true;
                    nimg.src = axr.images[i].src;
                    axr.images[i] = nimg;
                }
                
            }else
                axr.perLoaded[0]++;
        }
    }
    //check audio
    for(var j in axr.audio){
        //select only the created resources
        if(axr.audio[j].axisResource === true){
            axr.perLoaded[1]++;
            if(axr.audio[j].readyState < 3){
                allLoaded = false;
                if(axr.loadfails === 100){
                    axr.newSound(j, axr.audio[j].path, axr.audio[j].types, axr.audio[j].loop);
                }
            }else
                axr.perLoaded[0]++;
        }
    }
    //check video
    for(var j in axr.video){
        //select only the created resources
        if(axr.video[j].axisResource === true){
            axr.perLoaded[1]++;
            if(axr.video[j].readyState !== 4){
                allLoaded = false;
                if(axr.loadfails === 100){
                    axr.newVideo(j, axr.video[j].path, axr.video[j].width, 
                    				axr.video[j].height, axr.video[j].types);
                }
            }else
                axr.perLoaded[0]++;
        }
    }
    //check if tiles are all loaded, must go in and check every image
    for(var k in axr.tiles){
        if(axr.tiles[k].axisResource === true){
            for(var l in axr.tiles[k]){
            	if(!axr.tiles[k][l].src)
            		continue;
                axr.perLoaded[1]++;
                if(axr.tiles[k][l].width < 1 || !axr.tiles[k][l].ready){
                    allLoaded = false;
                    //if the amount of loadfails exceeds 100
                    //reload those elements
                    if(axr.loadfails === 100){
                        src = axr.tiles[k][l].src;
                        axr.tiles[k][l] = new Image();
                        axr.tiles[k][l].addEventListener("load", function(){
                            this.ready = true;
                        });
                        axr.tiles[k][l].src = src;
                    }
                    
                }else
                    axr.perLoaded[0]++;
                
            }    
        }
    }
    //if the load fails are >= 100 can reset timer
    //all things stalled have already been reloaded
    if(axr.loadfails > 100){
        axr.loadfails = 0;
    }
    //axr.perloaded does not exist yet on first run so must check if it does,
    //!allLoaded checks if the load check failed, if it did it sees if the difference
    //of the number of resources loaded and not loaded is the same as last time. if it is
    //it counts one up on axr.loadfails. regardless it updates the last difference
    if(!allLoaded && axr.perLoaded){
        if(axr.loadstalls === axr.perLoaded[1]-axr.perLoaded[0]){
            axr.loadfails++;
        }else{
        	axr.loadfails = 0;
        }
        
        axr.loadstalls = axr.perLoaded[1]-axr.perLoaded[0];
    }else if(allLoaded){
        //if the load check succeded then these should be zero
        axr.loadfails = 0;
        axr.loadstalls = 0;
    }
    
    return allLoaded;
    
};
//put in unlimited images in format [name,path]
axr.loadImages = function (){
    var arglen = arguments.length;
    var i = 0;
    var obj;
    while(i < arglen){
        obj = arguments[i++];
        var img = new Image();
        //attach a load event to set ready = true
        img.addEventListener("load", function(){
            this.ready = true;
        });
        img.src = obj[1];
        img.axisResource = true;
        axr.images[obj[0]] = img;
    }
};

axr.loadTiles = function(name, str, num, dontPad, onendfunc){
    var tileArray = [];
    var base = str.split('%d');
    var i = 0;

    while(i < num){
        var img = new Image();
        //do the same ready = true on load
        img.addEventListener("load", function(){
            this.ready = true;
        });
        
        img.src = base.join(axu.padNum(i,num));
            
        tileArray.push(img);
        i++;
    }
    tileArray.axisResource = true;
    axr.tiles[name] = tileArray;
    if(onendfunc){
        axr.tiles[name].onended = onendfunc;
    }
 
};

axr.imageToTiles = function(name, img, xcount, ycount, onendfunc){
    if(typeof(img) === "string"){
        var nimg = new Image();
        nimg.sprite = true;
        nimg.xcount = xcount;
        nimg.ycount = ycount;
        nimg.onendfunc = onendfunc;
        nimg.axisResource = true;
        nimg.addEventListener("load",
        function(){
            nimg.ready = true;
            axr.imageToTiles(name,nimg,xcount,ycount,onendfunc);
        });
        nimg.src = img;
        axr.images[name] = nimg;
        return;
    }
    var tiles = [];
    var width = Math.floor(img.width/xcount);
    var height = Math.floor(img.height/ycount);
    
    var xc = 0;
    var yc = 0;
    for(var i = 0,l = xcount*ycount; i < l;i++){
        
        var can = document.createElement('canvas').getContext('2d');
        can.canvas.width = width;
        can.canvas.height = height;
        can.drawImage(img, width*xc, height*yc, width, height, 0, 0,width,height);
        can.ready = true;
        tiles.push(can.canvas);
        
        if(xc === xcount-1){
            yc++;
            xc = 0;
        }else{
            xc++;
        }
    }
    
    axr.tiles[name] = tiles;
    if(onendfunc){
        axr.tiles[name].onended = onendfunc;
    }
};

axr.newSound = function(name,path,types,doloop){
    var sound = document.createElement('audio');
    if(doloop){
        sound.addEventListener("ended",function(){
            sound.play();
        });
        sound.loop = true;
    }
    sound.setAttribute("id", name);
    sound.path = path;
    sound.types = types;
    
    for(var i = 0; i < types.length; i++){
        var src = document.createElement('source');
        src.setAttribute("src",path+"."+ types[i]);
        var codec = (types[i] === "mp3") ? "mpeg" : "ogg"
        
        src.setAttribute("type", "audio/"+codec);
        sound.appendChild(src);
    }
    sound.axisResource = true;
    axr.audio[name] = sound;
    
};
axr.newVideo = function(name, path, width, height,types){
    var video = document.createElement('video');

    video.setAttribute("id", name);
    video.setAttribute("width", width);
    video.setAttribute("width", height);
    video.width = width;
    video.height = height;
    video.path = path;
    video.types = types;
    
    video.set = function(x,y){
    	video.setAttribute("style", "position:fixed;left:"+x+"px;top:"+y+"px;");
    	document.body.appendChild(video);
    };
    video.remove = function(){
    	var vid = document.getElementById(name);
    	document.body.removeChild(vid);
    };
    for(var i = 0; i < types.length; i++){
        var src = document.createElement('source');
        src.setAttribute("src",path+"."+ types[i]);
        src.setAttribute("type", "video/"+types[i]);
        video.appendChild(src);
    }
    video.axisResource = true;
    axr.video[name] = video;
    
};
