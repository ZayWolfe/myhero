var makeHero = function (){
    //there will only be a current location if the hero has excited
    //a city. If that's the case, spawn the target near the city
    if(mh.currentLocation){
        var cityPos = world.regions[mh.currentRegion].returnLocation(mh.currentLocation).pos;
        var position = [cityPos[0] + 20, cityPos[1] - 10];
    } else {
        var position = [90,-80];
    }
	var hero = new ax.Actor({
		stamp: axr.images.hero,
        pos: (mh.hero.actor && !mh.inCity)? mh.hero.actor.pos: position,
        width:11,
        height:20,
		layer:2,
        type: "hero",
        angle: axu.randBet(0,360)
	});
	hero.path = [
		[-ax.halfWidth, -ax.halfHeight],
		[ax.halfWidth, -ax.halfHeight],
		[ax.halfWidth, ax.halfHeight],
		[-ax.halfWidth, ax.halfHeight]
	];
    
    hero.gameScript = function (){
        axi.runThrough(hero, axi.b.hero);
        
        if(mh.hero.hp[0] > mh.hero.hp[1]){
            mh.hero.hp[0] = mh.hero.hp[1];
        }
        
        if(mh.hero.hp[0] < 0){
        mh.hero.hp[0] = 0;
        }
        
        if(mh.hero.hp[0] < 1){
            mh.heroDeath();
        }
        
        if(mh.hero.move){
            //some form of auto heal
            if(mh.hero.hp[0] < mh.hero.hp[1]){
                mh.hero.hp[0] += 0.01;
            }
        }
        if(mh.hero.rest){
            //resting heal
            if(mh.hero.hp[0] < mh.hero.hp[1]){
                mh.hero.hp[0] += 0.02;
            }
        }
    };
	mh.hero.actor = hero;
};
var makeTarget = function() {
    //there will only be a current location if the hero has excited
    //a city. If that's the case, spawn the target near the city
    if(mh.currentLocation){
        var cityPos = world.regions[mh.currentRegion].returnLocation(mh.currentLocation).pos;
        var position = [cityPos[0] + 20,cityPos[1]];
    } else {
        var position = [90,-80];
    }
    var target = new ax.Actor({
        stamp: axr.images.target,
        pos: (mh.target && !mh.inCity)? mh.target.pos: position,
        width: 10,
        height:10,
        layer:2
    });
    target.makeCollidable(6);
    target.gameScript = function(){
        
    };
    target.oncolliding = function(location){
        if(mh.hero.move &&
           axu.distance(location.pos, mh.hero.actor.pos) < 15){
            mh.currentLocation = location.name;
            mh.transition(true);
            ax.direct.playScene("city");
        }else {
            mh.hero.actor.angle = axu.direction(
                                                mh.hero.actor.pos,
                                                location.pos
                                                );
        }
        
    };
    
    mh.target = target;
};

