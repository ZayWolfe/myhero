//event code

var startLightEvent = function(){
    var chooser = axu.randBet(0, 5);
    var item = null;
    
    if(chooser < 2){
        mh.makeEventMsg(addHerb().name + " found.");
    } else if(chooser < 6){
        startBattle();
    }
};


var startBattle = function(){
    //set the hero to stop moving and pick an enemy. Need to make
    //a function to auto pick based on area and level but rat will do for now.
    mh.hero.move = false;
    var enemy = pickEnemy();
    var oldhp = enemy.hp;
    
    //set the message and pick whos turn is it
    mh.makeEventMsg("Attacked by " + enemy.name);
    var turn = axu.randBet(0, 1);
    
    //make the battle as a looping script until one of their health
    //reaches 0
    var bttlScpt = new ax.Scripter();
    bttlScpt.loop = true;
    bttlScpt.cmd(1, function(){
        //checking if either won
        if(mh.hero.hp[0]<=  0 || enemy.hp <= 0 ){
            mh.hero.move = true;
            //temporarily reset health
            enemy.hp = oldhp;
            //turn off scpt
            bttlScpt.kill();
            //battle end mesage
            if(mh.hero.hp[0] <= 1){
                mh.makeEventMsg("Hero has been defeated.");
                mh.heroDeath();
            }else {
                mh.makeEventMsg(
                    enemy.name + " has been defeated. "+
                    mh.giveBattleExp(enemy) + " exp gained."
                );
                
                //give item based on wonder level
                if(axu.probability(mh.hero.luck+mh.hero.wonder)){
                    giveRandomItems();
                }
               ;
            }
        } else {
            //based randomy on battle turns
            if(turn === 1){//enemy goes
                turn = 0;
                makeDamage(enemy, mh.hero);
                
            }else {//hero goes
                turn = 1;
                makeDamage(mh.hero, enemy);

            }
        }
    });
    bttlScpt.fire();
};

var makeDamage = function(attacker, taker){
    var atk = (attacker.str*(attacker.luck/10+1));
    var def = (taker.def*0.7*(taker.luck/10+1));
    
    var defItems = mh.searchInventory({category:"armor",equiped:true});
    var atkItems = mh.searchInventory({category:"weapons",equiped:true});
    if(attacker.type === "hero"){
        atkItems.map(function(item){
            atk += item.atk;
            def += item.def;
        });
    }
    if(taker.type === "hero"){
        defItems.map(function(item){
            atk += item.atk;
            def += item.def;
        });
    }
    
    //lucky hits
    if(axu.probability(taker.luck/3+taker.agi)){
        mh.makeEventMsg(taker.name + " dodges!");
        return;
    }
    if(axu.probability(attacker.luck/2+attacker.agi)){
        mh.makeEventMsg(attacker.name + " gets critical hit!");
        atk = atk + axu.randBet(0, attacker.luck);
    }
    
    var damage = Math.round(axu.keepPositive(atk - def));
    
    //compensate for hero's odd hp
    if(taker.actor){
        taker.hp[0] -= damage;
    }else {
        taker.hp -= damage;
    }
    if(damage > 0){
        mh.makeEventMsg(attacker.name + " does " +
        damage + " damage to " + taker.name + ".");
    } else {
        mh.makeEventMsg(taker.name + " blocks attack.");
    }
    
};
