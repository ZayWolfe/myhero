
//item generation and handleing
var addHerb = function(chooser){
    chooser = chooser || axu.randBet(0, 9);
    
    var herb = {
        id : "H"+axu.randBet(0,99999999999999999),
        count : 1,
        category : "misc",
        equipable : false,
        type : "herb"
    };
    if(chooser < 7){
        herb.name = "Health Herb +15";
        herb.action = function(){   
            mh.hero.hp[0] += 15;
        }
        herb.cost = 2;
    }else if(chooser < 8 ){
        herb.name = "Health Herb +30";
        herb.action = function(){
            mh.hero.hp[0] += 30;
        }
        herb.cost = 5;
    }else if(chooser < 10){
        herb.name = "Lavender";
        herb.cost = 10;
    }
    
    mh.addItemToInventory(herb);
    
    return herb;
};



var generateMaterial = function (){
    var level = mh.hero.lvl;
    var materialList = [];
    //wood
    if(level <= 2 || axu.probability(100-(level-3)*10)){
        materialList.push("Wooden");
    }
    //copper
    if(level > 2  && axu.probability(100-(level-5)*10)){
        materialList.push("Copper");
    }
    //iron
    if(level > 6 && axu.probability(100-(level-12)*10)){
        materialList.push("Iron");
    }
    //bronze
    if(level > 12 && axu.probability(100-(level-20)*10)){
        materialList.push("Bronze");
    }
    //steel
    if(level > 21 && axu.probability(100-(level-30)*10)){
        materialList.push("Steel");
    }
    if(level > 31 && axu.probability(100-(level-50)*10)){
        materialList.push("Silver");
    }
    
    
    if(materialList.length < 1){
        return generateMaterial();
    }else {
        return materialList[axu.randBet(0, materialList.length-1)];
    }
};

var generateWeaponType = function (){
    var types = [
        "bow",
        "bow",
        "short-sword",
        "short-sword",
        "sword",
        "sword",
        "sword",
        "hachet",
        "hachet",
        "hachet",
        "hachet",
        "axe",
        "dagger",
        "spear",
        "dagger",
        "spear",
        "dagger",
        "spear",
        "dagger",
        "spear",
        "katana",
        "hammer",
        "mace",
        "mace",
        "mace",
        "mace"
    ];
    
    return types[axu.randBet(0, types.length-1)];
};

var generateWeapon = function(material, type){
    material = material || generateMaterial();
    type = type || generateWeaponType();
    var weapon = {
        agi:0,
        atk:0,
        def:0,
        spd:0,
        equipable: true,
        equiped: false,
        category: "weapons",
        type: type,
        place: "arm",
        id: "W"+axu.randBet(0,99999999999999999),
        count: 1
    };
    var level = mh.hero.lvl;
    
    //determine the material effect on weapon stats
    var atk = Math.round(level/4);
    var cost = 20;
    if(material === "Wooden"){
        atk += axu.randBet(1,3);
        cost += 5;
    } else if(material === "Copper"){
        atk += axu.randBet(3,7);
        cost += 20;
    }else if(material === "Iron"){
        atk += axu.randBet(7,10);
        cost += 50;
    }else if(material === "Bronze"){
        atk += axu.randBet(10,18);
        cost += 100;
    }else if(material === "Steel"){
        atk += axu.randBet(18,30);
        cost += 180;
    }else if(material === "Steel"){
        atk += axu.randBet(30,50);
        cost += 250;
    }
    //determine type effects on weapon stats
    if(type === "bow"){
        weapon.agi = axu.randBet(0, Math.round(level/2));
        weapon.def = -1*axu.randBet(1, Math.round(level/4));
        atk += axu.randBet(0, level - Math.round(level/3));
        cost += 10;
    } else if(type === "short-sword"){
        weapon.agi = axu.randBet(0, level - Math.round(level/4));
        weapon.def = axu.randBet(0, Math.round(level/3));
        cost += 15;
    } else if(type === "sword"){
        atk += axu.randBet(0, level - Math.round(level/4));
        cost += 20;
    }else if(type === "hatchet"){
        weapon.agi = axu.randBet(0, Math.round(level/2));
        atk += axu.randBet(0, level - Math.round(level/4));
        cost += 15;
    } else if(type === "dagger"){
        atk += axu.randBet(0, level - Math.round(level/5));
        weapon.agi = axu.randBet(0, level - Math.round(level/2));
        cost += 10;
    }else if(type === "spear"){
        weapon.agi = axu.randBet(0, Math.round(level/4));
        atk += axu.randBet(0, level - Math.round(level/4));
        cost += 15;
    }else if(type === "mace"){
        weapon.spd = -1;
        weapon.agi = -1*axu.randBet(1, axu.keepPositive(mh.hero.agi-4));
        atk += axu.randBet(0, level - Math.round(level/4));
        cost += 15;
    }else if(type === "axe"){
        weapon.spd = -1*axu.randBet(1, 2);
        weapon.agi = -1*axu.randBet(1, axu.keepPositive(mh.hero.agi-3));
        atk += axu.randBet(1, level - Math.round(level/3));
        cost += 30;
        weapon.place = "arms"; //must enable these when two handed equip is set up
    }else if(type === "hammer"){
        weapon.spd = -1*axu.randBet(1,3);
        weapon.agi = -1*axu.randBet(1, axu.keepPositive(mh.hero.agi-2));
        atk += axu.randBet(0, level - Math.round(level/2));
        cost += 40;
        weapon.place = "arms";
    }else if(type === "katana"){
        weapon.agi = axu.randBet(0, Math.round(level/3));
        atk += axu.randBet(1, level - Math.round(level/2));
        weapon.spd = 1;
        cost += 50;
        weapon.place = "arms";
    }
    
    //make sure the hero can always move
    if(mh.hero.spd + weapon.spd < 1){
        weapon.spd += Math.abs(mh.hero.spd + weapon.spd);
        wepons.spd += 1;
    }
    //putting it together
    weapon.name = material + " " + type;
    weapon.cost = cost;
    weapon.atk = atk;
    
    return weapon;    
};

var addWeapon = function(material, type) {
    var weapon = generateWeapon(material, type);
    mh.addItemToInventory(weapon);
    
    return weapon;
};

var giveRandomItems = function (){
    var limit = Math.ceil((mh.hero.wonder+mh.hero.luck/2)/10);
    for(var i = 0; i < limit; i++){
        var chooser = axu.randBet(0,10);
        if(chooser < 5){
            mh.makeEventMsg(addHerb().name + " found.");
        }else if (chooser < 8){
            var getGold = axu.randBet(1, Math.round(mh.hero.lvl+mh.hero.luck/2))
            mh.makeEventMsg("Found " + getGold + " gold.");
            mh.hero.gold += getGold;
        }else {
            mh.makeEventMsg(addWeapon().name + " found.");
        }//need to add materials and armor to items findable
    }
    mh.saveGameData();
};