/*
* Main file for MyHero html5,
*
* info:
* LAYERS:
* layer 0 is for maps, layer 1 is for locations, layer 2 is for the hero
* and gameplay icons like status or markers, layer 3 is an overlay for the map
* and for like weather effects and stuff, layer 4 is for the interface.
*/
//global vars

mh.map = null;
mh.aScreen = null;
mh.eScreen = null;
mh.cScreen = null;
mh.currentLocation = null;
mh.currentRegion = null;
mh.inCity = false;
mh.hero = {};

mh.loop = function (){
	if(axe.mouse.leftUp){
		mh.target.pos[0] = axe.mouse.pos[0]-ax.scrollx;
		mh.target.pos[1] = axe.mouse.pos[1]-ax.scrolly;
	}
};

mh.generateBaseHero = function(){
	 mh.hero = {
        name: "Hero",
        actor: null,
        courage: 3,
        wonder: 3,
		barter: 0,
		unarmed: 0,
		blade:0,
		missile:0,
		blunt:0,
        hp: [100,100],
        energy:[100,100],
        age: 17,
        str: 3,
        def: 3,
        agi:3,
        luck:3,
        spd:5,
        lvl:1,
        move:true,
        rest:false,
        inv:[],
        gold: 20,
        skillPoints:0,
        exp:0,
        equipment: {
            arm1:null,
            arm2:null,
            body:null,
			feet:null,
            neck:null
        }
    }; 
    addWeapon();//temporary needed weapon
    addHerb();//temporary needed items
    addHerb();
    addHerb();
    addHerb();
    addHerb();
};

mh.giveExperience = function(num){
	var experience = mh.hero.exp+num;
	var level = mh.hero.lvl;
	
	var expNeeded = Math.round(50*(Math.pow(1.35, level)))-47;
	
	if(experience >= expNeeded){
		mh.hero.lvl += 1;
		mh.hero.exp = Math.round(
			axu.keepPositive(experience-expNeeded)
		);
		
		mh.hero.skillPoints += axu.randBet(3,4);//give skill points
		mh.saveGameData();//save game
	} else {
		mh.hero.exp = experience;
	}
	
};

mh.giveBattleExp = function (enemy){
	var level = mh.hero.lvl;
	var exp = 0;
	
	if(level < enemy.lvl){
		exp = axu.randBet(9, 10*(enemy.lvl + level));
	} else if(level === enemy.lvl){
		exp = level * 5;
	}else {
		exp = 1;
	}
	mh.giveExperience(exp);
	return exp;
}

//right now it's simple sessionStorage, need to open it up more
//later for possibilities to include facebook account storage or
//even accounts from iris-studios
mh.loadGameData = function (){
	var saveString = sessionStorage.getItem("myhero");
	if(saveString){
		var saveData = JSON.parse(saveString);
		mh.hero = saveData.hero;
		//world.regions = saveData.regions;
		mh.currentLocation = saveData.currentLocation;
		mh.currentRegion = saveData.currentRegion;
		mh.repairInventory();//repair action items
		
		return true;
	} else {
		return false;
	}
};

mh.makeSaveObject = function() {
	var obj = {};
	
	obj.hero = {
        name: mh.hero.name,
        courage: mh.hero.courage,
        wonder: mh.hero.wonder,
		barter: mh.hero.barter,
		unarmed: mh.hero.unarmed,
		blade: mh.hero.blade,
		missile: mh.hero.missile,
		blunt: mh.hero.blunt,
        hp: mh.hero.hp,
        energy: mh.hero.energy,
        age: mh.hero.age,
        str: mh.hero.str,
        def: mh.hero.def,
        agi: mh.hero.agi,
        luck: mh.hero.luck,
        spd: mh.hero.spd,
        lvl: mh.hero.lvl,
        move: true,
        rest: false,
        inv: mh.hero.inv,
        gold: mh.hero.gold,
        skillPoints: mh.hero.skillPoints,
        exp: mh.hero.exp,
        equipment: mh.hero.equipment
	};
	obj.currentLocation = mh.currentLocation;
	obj.currentRegion = mh.currentRegion;
	//obj.regions = world.regions; //find way to save world later
	
	return obj;
};

mh.saveGameData = function (){	
	try {
		var saveString = JSON.stringify(mh.makeSaveObject());
		
	} catch(err) {
		throw err;
	}
	sessionStorage.setItem("myhero", saveString);
};

mh.getTime = function (){
	return (new Date).getHours() + ":" + (new Date).getMinutes();
};
/*It's necesary to repair certain items with actions in the inventory
*after loading a saved game since the actions of each item aren't
*preserved in json.
*
*Just fixes herbs now but must eventually fix all action items.
*/
mh.repairInventory = function(){
    var inventory = mh.hero.inv;
    for(iterator in inventory){
        var item = inventory[iterator];
        //repair just herbs
        if(item.type === "herb"){
            if(item.name === "Health Herb +15"){
                item.action = function(){
                    mh.hero.hp[0] += 15;
                }
            }
            if(item.name === "Health Herb +30"){
                item.action = function(){
                    mh.hero.hp[0] += 30;
                }
            }
        }
    }
}

mh.removeItemFromInventory = function(item){
	for(var i = 0, l = mh.hero.inv.length; i < l; i++){
		var item2 = mh.hero.inv[i];
		if(item.name === item2.name &&
		   item.cost === item2.cost &&
		   item.equipable === item2.equipable &&
		   item.id === item2.id){
			if(mh.hero.inv[i].count > 1){
				mh.hero.inv[i].count -= 1;
			}else {
				mh.hero.inv[i] = null;
			}
			break;
		}
	}
	
};
mh.searchInventory = function (object) {
	//filter out the items based on criteria or pass them if not given
	return mh.hero.inv.filter(function(item){
		var name = (object.name)? object.name: item.name;
		var type = (object.type)? object.type: item.type;
		var category = (object.categoty)? object.category: item.category;
		var id = (object.id)? object.id: item.id;
		var equiped = (object.equiped)? object.equiped: item.equiped;

		return (
				item.name === name &&
				item.type === type &&
				item.category === category &&
				item.id === id &&
				item.equiped === equiped
				);
	});
	
};
mh.addItemToInventory = function(item){
	//check to see if the item already exists
	if(item.category !== "weapons"){
		var existList = mh.searchInventory({
			name : item.name,
			type : item.type,
			category : item.category
		});
		if(existList.length > 0){ //if the item already exists in inventory just increment the cound
			existList[0].count += 1;
		} else {//otherwise add it to inventory
			mh.hero.inv.push(item);
		}
	}else { //weapons are more complicated, just add it without changing the count for now
		mh.hero.inv.push(item);	
	}
	
};
mh.useItem = function(item) {
	//if there is an action, then it's a disposable item
	if(item.action){
		item.action();
		mh.makeEventMsg(item.name + " has been used.");
		mh.removeItemFromInventory(item);
		
	} else if(item.equipable){ //if the item can be equiped
		mh.equipItem(item);
		
	}
};
mh.equipItem = function(item){
	var eqpmt = mh.hero.equipment;
	//first check if the item is equiped
	if(item.equiped){
		//arm placed weapons
		if(item.place === "arm" || item.place === "arms"){
			if(eqpmt.arm1 && eqpmt.arm1.id === item.id){
				item.equiped = false;
				eqpmt.arm1 = null;
				mh.makeEventMsg(item.name + " removed from right arm.");
			}
			if(eqpmt.arm2 && eqpmt.arm2.id === item.id){
				item.equiped = false;
				eqpmt.arm2 = null;
				mh.makeEventMsg(item.name + " removed from left arm.");
			}
		}
	}else { //it's not equiped, equip it
		if(item.place === "arms"){
			if(eqpmt.arm1){
				mh.makeEventMsg("Weapon already equiped on right arm.");
			}else {//if it's false it's empty and something can possibly be equiped
				if(eqpmt.arm2){
					mh.makeEventMsg("Weapon already equiped on left arm.");
				}else {//if it's false it's empty too and something can be equiped
					//add item to both hands
					mh.makeEventMsg(item.name + " equiped on both arms.");
					eqpmt.arm2 = item;
					eqpmt.arm1 = item;
					item.equiped = true;
				}
				
			}
			
		}
		if(item.place === "arm"){
			
			if(eqpmt.arm1){
				if(eqpmt.arm2){
					//if both return true, both arms are already full
					mh.makeEventMsg("Both arms are already equiped.");
				}else {//if it's false it's empty and something can be equiped
					mh.makeEventMsg(item.name + " equiped on left arm.");
					eqpmt.arm2 = item;
					item.equiped = true;
				}
			}else {//if it's false it's empty and something can be equiped
				mh.makeEventMsg(item.name + " equiped on right arm.");
				eqpmt.arm1 = item;
				item.equiped = true;
			}
		}
	}
};

mh.transition = (function(){
	var scripters = null;
	var events = null;
	
	return function(mode){
		if(mode){ //if true is passed then save scripters
			//save events
			if(mh.eScreen){
				events = mh.eScreen.events;
			}
			scripters = [];
			ax.everyActor(function(act){
				if(act.id[0] === "S"){
					scripters.push(act);
				}
			});
		} else if(scripters || events){//else restore them
			for(key in scripters){
				if(scripters[key].id){
					if(!ax.playarea[0]){
						ax.playarea[0] = [];
					}
					ax.playarea[0].push(scripters[key]);
				}
			}
			scripters = null;
			if(events){
				if(mh.eScreen){
					mh.eScreen.events = events;
					events = null;
				}
			}
		}
	};
})();

mh.heroDeath = function(){
	mh.hero.hp[0] = 100;
	mh.hero.move = true;
	ax.direct.playScene("city");
};

mh.makeEventMsg = function(msg){
	msg = msg + " (" + mh.getTime() + ")";
	mh.eScreen.addEvent(msg);
};


function init(){
    axr.loadImages(
		["world","graphics/map.png"],
		//interface
		["announce","graphics/interface/announce.png"],
		["inventory","graphics/interface/inventory.png"],
		["events","graphics/interface/events.png"],
		["announceC","graphics/interface/announce-c.png"],
		["eventsC","graphics/interface/events-c.png"],
		["char","graphics/interface/char.png"],
		["button","graphics/interface/button.png"],
		["button2","graphics/interface/button2.png"],
		["list","graphics/interface/list.png"],
		["list-e","graphics/interface/list-e.png"],
		["confirm", "graphics/interface/confirm.png"],
		["bar", "graphics/interface/bar.png"],
		//city stuff
		["cityBack", "graphics/interface/cityback.png"],
		["cityWindow", "graphics/interface/citywindow.png"],
		["cityImg1", "graphics/interface/cityimage.jpg"],
		["citybutton", "graphics/interface/marblebutton.png"],
		["citybuttonlit", "graphics/interface/marblebuttonlit.png"],
		//icons
		["saveIcon", "graphics/interface/save-icon.png"],
		["saveClear", "graphics/interface/saveClear.png"],
		["cancelIcon", "graphics/interface/cancel.png"],
		["charIcon", "graphics/interface/char-icon.png"],
		//overlays
		["creepForest","graphics/overlays/cforest.png"],
		["sunkSwamp","graphics/overlays/sswamp.png"],
		["mirDesert","graphics/overlays/mdesert.png"],
		["peekPlains","graphics/overlays/pplains.png"],
		["starLake","graphics/overlays/slake.png"],
		["desineSea","graphics/overlays/dsea.png"],
		["crystalBeach","graphics/overlays/cbeach.png"],
		//icons
		["target","graphics/target.png"],
		["hero","graphics/hero.png"],
		["testCity", "graphics/city.png"]
	);
	//loading audio
	axr.newSound("village", "audio/DST-VillagersSong", ["mp3","ogg"], true);
	axr.newSound("town", "audio/medieval", ["mp3","ogg"], true);
	axr.newSound("character", "audio/Azum", ["mp3","ogg"], true);
    
    ax.width = 700;
    ax.height = 500;
    ax.lockRes = true;
    ax.Init(30);
    ax.setBack("#888");
	//ax.debug = true;
	ax.direct.playScene("loading");
}




