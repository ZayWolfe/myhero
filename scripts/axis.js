/*Axis engine core
* “The greatest danger for most of us is not that our aim is too high
* and we miss it, but that it is too low and we reach it.” - Michelangelo
*/
var BrowserDetect = {
	init: function () {
		this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
		this.version = this.searchVersion(navigator.userAgent)
			|| this.searchVersion(navigator.appVersion)
			|| "an unknown version";
		this.OS = this.searchString(this.dataOS) || "an unknown OS";
	},
	searchString: function (data) {
		for (var i=0;i<data.length;i++)	{
			var dataString = data[i].string;
			var dataProp = data[i].prop;
			this.versionSearchString = data[i].versionSearch || data[i].identity;
			if (dataString) {
				if (dataString.indexOf(data[i].subString) != -1)
					return data[i].identity;
			}
			else if (dataProp)
				return data[i].identity;
		}
	},
	searchVersion: function (dataString) {
		var index = dataString.indexOf(this.versionSearchString);
		if (index == -1) return;
		return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
	},
	dataBrowser: [
		{
			string: navigator.userAgent,
			subString: "Chrome",
			identity: "Chrome"
		},
		{ 	string: navigator.userAgent,
			subString: "OmniWeb",
			versionSearch: "OmniWeb/",
			identity: "OmniWeb"
		},
		{
			string: navigator.vendor,
			subString: "Apple",
			identity: "Safari",
			versionSearch: "Version"
		},
		{
			prop: window.opera,
			identity: "Opera",
			versionSearch: "Version"
		},
		{
			string: navigator.vendor,
			subString: "iCab",
			identity: "iCab"
		},
		{
			string: navigator.vendor,
			subString: "KDE",
			identity: "Konqueror"
		},
		{
			string: navigator.userAgent,
			subString: "Firefox",
			identity: "Firefox"
		},
		{
			string: navigator.vendor,
			subString: "Camino",
			identity: "Camino"
		},
		{		// for newer Netscapes (6+)
			string: navigator.userAgent,
			subString: "Netscape",
			identity: "Netscape"
		},
		{
			string: navigator.userAgent,
			subString: "MSIE",
			identity: "Explorer",
			versionSearch: "MSIE"
		},
		{
			string: navigator.userAgent,
			subString: "Gecko",
			identity: "Mozilla",
			versionSearch: "rv"
		},
		{ 		// for older Netscapes (4-)
			string: navigator.userAgent,
			subString: "Mozilla",
			identity: "Netscape",
			versionSearch: "Mozilla"
		}
	],
	dataOS : [
		{
			string: navigator.platform,
			subString: "Win",
			identity: "Windows"
		},
		{
			string: navigator.platform,
			subString: "Mac",
			identity: "Mac"
		},
		{
			   string: navigator.userAgent,
			   subString: "iPhone",
			   identity: "iPhone/iPod"
	    },
		{
			string: navigator.platform,
			subString: "Linux",
			identity: "Linux"
		}
	]

};


/* Axis variables for scenegraphs + system settings
*/
var ax = {};
ax.width = null;
ax.height= null;
ax.halfWidth = null;
ax.halfHeight = null;
ax.scrollx = null;
ax.scrolly = null;
ax.level  = null;
ax.cleanupRate = 300;
//for locking aspect ratio
ax.lockRes = false;
ax.lockDiv = {};
ax.lockDiv.width = 0;
ax.lockDiv.height = 0;
ax.resize = false;
//drawing layers
ax.playarea = [];
ax.menu = []; //scenegraph for things that must be on top always
ax.windowActive = true;
//ax simple cmds
// Quick setting the background of the canvas
ax.setBack = function(code){
    ax.canvas.style.backgroundColor = code;
};
//used when needed to iterate over every actor for some reason
ax.everyActor = function (func){
    var layer = null;
    var act = null;
    for(var sglength = ax.playarea.length, sgi = 0; sgi < sglength; sgi++){
        layer = ax.playarea[sgi];
        if(!layer){
            ax.playarea[sgi] = [];
            continue;
        }
        for(var l = layer.length, i = 0; i < l; i++){
            act = layer[i];
            if(act){
               func(act)
            }
        }
    }
    
    //getting actors frommenu layer
    for(var l = ax.menu.length, i = 0; i < l; i++){
        act = ax.menu[i];
        if(act){ //select only unkilled actors
            func(act);
        }
    }
};

ax.cleanActors = (function(){
    var cnt = 0;
    return function(){
        
        if(cnt >= ax.cleanupRate){
            cnt = 0;
            //clean up and fix reference points for scenegraph layers
            for(var j = 0,layerLength = ax.playarea.length; j<layerLength; j++){
                ax.playarea[j] = ax.playarea[j].filter(function(act){return (act !== null)});
                for(var i = 0, l = ax.playarea[j].length; i<l; i++){
                    ax.playarea[j][i].changeRef(i);
                }
            }
            //clean up and fix reference points for menu layer
            ax.menu = ax.menu.filter(function(act){return (act !== null)});
            for(var i = 0, l = ax.menu.length; i<l; i++){
                ax.menu[i].changeRef(i);
            }
        }
        cnt++;
    };
})();

// Create game screen and/or resolution locking div
ax.makeGame = function(){
    if(ax.lockRes){
        var lock = document.createElement('div');
        lock.setAttribute("id", "lockingDiv");
        ax.lockDiv = lock;
        lock.setAttribute("style","position:absolute;left:50%;top:50%;");
        
        var game = document.createElement('canvas');
        game.setAttribute("id","gameScreen");
        game.setAttribute("style","width:100%;height:100%;");
        
        lock.appendChild(game);
        document.body.appendChild(lock);
        ax.canvas = game;
        ax.ctx = game.getContext('2d');
    }else{
        var game = document.createElement('canvas');
        game.setAttribute("id","gameScreen");
        document.body.appendChild(game);
        ax.canvas = game;
        ax.ctx = game.getContext('2d');
    }
};
// Function sets the diminsions of the game
ax.setDiminsions = function(){
    if(ax.width && ax.height && !ax.resize){
        ax.halfWidth = ax.width/2;
        ax.halfHeight = ax.height/2;
        ax.canvas.width = ax.width;
        ax.canvas.height = ax.height;
        ax.scrollx = ax.width/2;
        ax.scrolly = ax.height/2;
    }
    if(ax.resize || ax.lockRes){//autostretch game
        ax.resizeCanvas();
    } 
    if(!ax.resize && !ax.lockDiv && (!ax.width || !ax.height)){
        throw "Must set Axis to resize or set diminsions manually."
    }
    
    window.addEventListener("resize",ax.resizeCanvas);
};
// the function that resizes or stretches the canvas right
ax.resizeCanvas = function(){
    var sdim = [window.innerWidth,window.innerHeight];
    if(ax.lockRes){
        var ratio = ax.width/ax.height;
        var newRatio = sdim[0]/sdim[1];
        
        if(newRatio > ratio){
            sdim[0] = sdim[1] * ratio;
        } else {
            sdim[1] = sdim[0] / ratio;
        }
        
        //setting the dimensions in vars to translate mouse
        ax.lockDiv.width = sdim[0];
        ax.lockDiv.height = sdim[1];
        //setting dimensions
        ax.lockDiv.style.width = sdim[0] +"px";
        ax.lockDiv.style.height = sdim[1] +"px";
        ax.lockDiv.style.marginTop = (-sdim[1] / 2) + "px";
        ax.lockDiv.style.marginLeft = (-sdim[0] / 2) + "px";
    } else if(ax.resize){
        //set canvas to resolution of window
        ax.width = sdim[0];
        ax.height = sdim[1];
        ax.halfWidth = ax.width/2;
        ax.halfHeight = ax.height/2;
        ax.canvas.width = ax.width;
        ax.canvas.height = ax.height;
        ax.scrollx = ax.halfWidth;
        ax.scrolly = ax.halfHeight;
    }
    
}


ax.newWorld2 = function(x,y,image){
    //for now there's just a basic background/overlay
    //what apalia will be using is just really a big image. 
    //In the future a tile array will also need to be used for tiled
    //maps, not a priority now though
    var width = image.width;
    var height = image.height;
    
    var patterns = [];
    
    var pat = ax.ctx.createPattern(image, "no-repeat");
    
    var i = 0;
    //var l = levels.length;
    
    var sx,sy,px,py;
    
    //making return object
    var robj = {};
    robj.level = 1;
    robj.pos = [x,y];
    robj.width = width;
    robj.height = height;
    robj.draw1 = function (){
        ax.ctx.save();
        ax.ctx.translate(ax.scrollx, ax.scrolly);
        ax.ctx.fillStyle = pat;
        ax.ctx.fillRect(-ax.scrollx,-ax.scrolly,ax.width,ax.height);
        ax.ctx.restore();
    };
    robj.draw2 = function (){

    };
//Return the level object and set it to a system variable.
    ax.level = robj;
    return robj;
};

ax.newWorld = function(x,y,levels){
    //for now there's just a basic background/overlay
    //what apalia will be using is just really a big image. 
    //In the future a tile array will also need to be used for tiled
    //maps, not a priority now though
    var width = levels[0].width;
    var height = levels[0].height;
    
    var i = 0;
    var l = levels.length;
    
    var sx,sy,px,py;
    
    //making return object
    var robj = {};
    robj.level = 1;
    robj.pos = [x,y];
    robj.dims = [width,height];
    robj.draw1 = function (){
        i = 0;
        /*while(i < robj.level){
            wrl = levels[i];
            axd.image(false, wrl, robj.pos[0], robj.pos[1]);
            i++;
        }*/
        sx = robj.pos[0]+width/2-ax.scrollx;
        sx = (sx+ax.width/2 > width)?width-ax.width/2:sx;
        sy = robj.pos[1]+height/2-ax.scrolly;
        sy = (sy+ax.height/2 > height)?height-ax.height/2:sy;
        px = robj.pos[0]+ax.width/2;
        py = robj.pos[1]+ax.height/2;
        //round them
        sx = Math.round(sx);
        sy = Math.round(sy);
        px = Math.round(px);
        py = Math.round(py);
        while(i < robj.level){
            axd.sprite(true, levels[i++], sx, sy, ax.width, ax.height, px, py, ax.width, ax.height);
        }

        };
    robj.draw2 = function (){
        i = robj.level;
        while(i < l){
            axd.sprite(true, levels[i++], sx, sy, ax.width, ax.height, px, py, ax.width, ax.height);
        }

    };
//Return the level object and set it to a system variable.
    ax.level = robj;
    return robj;
};

//camera object


ax.camera = (function(){
    //hidden varibles
    var mt,moveout = null;
    //making camera object
    self = {};
    self.pos = [0,0];
    self.vel = [0,0];
    self.target = null;
    self.edgeLock = false;
    self.reset = function (){
        self.target = null;
        self.edgeLock = false;
        self.pos[0] = 0;
        self.pos[1] = 0;
        self.vel[0] = 0;
        self.vel[1] = 0;
    };
    self.update = (function (){
        var pos = self.pos;
        var vel = self.vel;
        var targer = null;
        var dis,disx,disy = 0;
        
        //function that limits the camera to the edges of a level
        var limitScroll = function(){//should optimize later
            var sx = ax.halfWidth-pos[0];
            var sy = ax.halfHeight-pos[1];
            var hwidth = ax.halfWidth;
            var hheight = ax.halfHeight;
    
            if(Math.ceil(sx-hwidth) >= ax.level.dims[0]/2-hwidth){
                    sx = ax.level.dims[0]/2;
            }else if(Math.ceil(sx-hwidth) <= (ax.level.dims[0]/2-hwidth)*-1){
                    sx = (ax.level.dims[0]/2-hwidth)*-1+hwidth;
            }
            if(Math.ceil(sy-hheight) >= ax.level.dims[1]/2-hheight){
                    sy = ax.level.dims[1]/2;
            }else if(Math.ceil(sy-hheight) <= (ax.level.dims[1]/2-hheight)*-1){
                    sy = (ax.level.dims[1]/2-hheight)*-1+hheight;
            }
            pos[0]= (sx-hwidth)*-1;
            pos[1]= (sy-hheight)*-1;
    
        };
        
        return function (){
            //update position from speed or target
            target = ax.camera.target;
            if(mt){
                disx = mt[0] - pos[0];
                disy = mt[1] - pos[1];
                dis = Math.sqrt(Math.pow(disx,2)+Math.pow(disy,2));
                
                if(dis < 20){
                    self.vel[0] = (disx)*0.3;
                    self.vel[1] = (disy)*0.3;
                }else {
                    self.vel[0] = (disx)*0.1;
                    self.vel[1] = (disy)*0.1;
                }
                //ending the moving
                if(dis < 1){
                    mt = null;
                    self.vel[0] = 0;
                    self.vel[1] = 0;
                    if(self.onarrive){
                        self.onarrive();
                    }
                }
                pos[0] += vel[0];
                pos[1] += vel[1];
            
            } else if(target){
                pos[0] = target.pos[0];
                pos[1] = target.pos[1];
            }else {
                pos[0] += vel[0];
                pos[1] += vel[1];
            }
            //update scroll values
            if(ax.camera.edgeLock){
                if(!ax.level){
                    throw "Camera is edge locked, but there's no level set up to lock to.";
                }
                limitScroll();
            } //else {
                ax.scrollx = ax.halfWidth-pos[0];
                ax.scrolly = ax.halfHeight-pos[1];
            //}
            
    
        };
    })();
    self.draw = (function (){
        var pos = self.pos;
        
        return function (){
            //draw debug camera
            if(ax.debug){
                axd.cir(false,pos[0],pos[1],4,null,[1,250,0,250]);
                axd.rect(false,pos[0],pos[1],10,10,null,[1,250,0,250]);
            }
        };
    })();
    self.moveto = (function(){
        return function (xy,speed){
            if(!xy){
                self.vel[0] = 0;
                self.vel[1] = 0;
                if(self.onarrive){
                    self.onarrive();
                }
            } else {
                xy[0] = Math.floor(xy[0]);
                xy[1] = Math.floor(xy[1]);
                mt = xy;
            }
        };
    })();
    return self;
})();


/*
 * New improved actor creation, using better vector physics and
 * initialization object. initialize like so:
 * new ax.Actor({
 *      type: "enemy",
*      pos: [100,200],
*      vel: [2,3],
*      width: 50,
*      height:50,
*      layer:1,
*      collideRadias: 25
 * });
*/
ax.Actor = function(initObject){//change front to layers later
    //default values
    initObject = initObject || {};
    initObject.pos = initObject.pos || [0,0];
    initObject.vel = initObject.vel || [0,0];
    initObject.speed = initObject.speed || axu.getLength(initObject.vel);
    initObject.angle = initObject.angle || axu.getAngle(initObject.vel,true);
    initObject.layer = initObject.layer || 0;
    initObject.width = initObject.width || 50;
    initObject.height = initObject.height || 50;
    initObject.type = initObject.type || "blank";
    initObject.collideRadius = initObject.collideRadius || 0;
    initObject.rotation = initObject.rotation || 0.0;
    initObject.parent = initObject.parent || null;
    initObject.path = initObject.path || null;
    initObject.aniName = initObject.aniName || null;
    initObject.currentFrame = initObject.currentFrame || 0;
    initObject.stamp = initObject.stamp || null;
    initObject.fixed = initObject.fixed || false;
    
    //sometimes a negative value is desired
    if(initObject.alpha === undefined)
        initObject.alpha = 1.0;
    if(initObject.state === undefined)
        initObject.state = 1;
    
    var self = this;
    //behind the scenes stuff
    var collideRef = null;
    var sceneRef = null;
    var animation; //check if this is being used
    var moveto;
    var ani = null;
    self.onScreen = false;//check if this is being used
    
    //properties
    self.id = "A"+axu.randBet(0,99999999999999999);
    self.type = initObject.type;
    self.width = initObject.width;
    self.height = initObject.height;
    self.pos = initObject.pos;
    self.vel = initObject.vel;
    self.angle = initObject.angle;
    self.speed = initObject.speed;
    self.normal = axu.getNormal(self.vel);
    self.layer = initObject.layer;
    self.collideRadius = initObject.collideRadius;
    self.alpha = initObject.alpha;
    self.rotation = initObject.rotation;
    self.state = initObject.state;
    self.parent = initObject.parent;
    self.path = initObject.path;
    self.aniName = initObject.aniName;
    self.currentFrame = initObject.currentFrame;
    self.stamp = initObject.stamp;
    self.fixed = initObject.fixed;
    
    //text
    self.text = {
        txt: "",
        font: "serif",
        weight: "",
        size: 12,
        color: "black",
        offset: [0,0],
        width: self.width,
        height: self.height,
        spacing: 15
    };
    self.drawText = (function (){
        var cache,otext,ofont,osize,oweight,ocolor,owidth,oheight,ospacing;
        var text,font,size,weight,color,width,height,spacing;
        return function (){
            text = self.text.txt;
            font = self.text.font;
            size = self.text.size;
            color = self.text.color;
            width = self.text.width;
            height = self.text.height;
            weight = self.text.weight;
            spacing = self.text.spacing;
            
            if(otext !==  text ||
                ofont !==  font ||
                osize !==  size ||
                ocolor !== color ||
                owidth !== width ||
                oheight !== height ||
                oweight !== weight ||
                ospacing !== spacing){
                
                otext = text;
                ofont = font;
                osize = size;
                ocolor = color;
                owidth = width;
                oheight = height;
                oweight = weight;
                ospacing = spacing;
                
                cache = axu.makeTxtImg(self.text);                
            }
            axd.image(true, cache, self.text.offset[0], self.text.offset[1]);
            return cache;
        };
    })();
    
    //actor methods
    //adds actor to the collide list and sets up collision properties
    self.makeCollidable = function (radius){
        if(!radius){
            throw "please supply a radius for actor id: "+self.id;
        }
        self.collideRadius = radius;
        if(!collideRef){
            if(self.fixed){
                collideRef = axc.addCollider(self,[self.pos[0]+ax.scrollx,self.pos[1]+ax.scrolly],radius);
            }else {
                collideRef = axc.addCollider(self,self.pos,radius);
            }
            return collideRef;
        }else{
            axc.collisions[collideRef].rad = radius;
            return undefined;
        }
    };
    //change animation and/or frame
    self.changeAni = function(name,frame){
        if(!name){
          self.aniName = null;
          self.currentFrame = 0;
          return;
        }
        self.aniName= name;
        ani = axr.tiles[self.aniName];
        self.currentFrame = frame || 0;
    };

    self.update = (function(){
        var pos = self.pos,vel = self.vel;
        var c,poly,opos,ovel,angle,speed,dis,moveout,numframe = 0;
        
        return function (){
            //set the old positions and contex if not already set
            if(!c)c = ax.ctx;
            if(!opos){
                opos=[0,0];
                opos[0] = pos[0];
                opos[1] = pos[1];
            }
            if(!ovel){
                ovel=[0,0];
                ovel[0] = vel[0];
                ovel[1] = vel[1];
            }
            poly = (poly !== self.path)? self.path: poly;
            angle = (angle !== self.angle)? self.angle:angle;
            speed = (speed !== self.speed)? self.speed:speed;
            //update normal
            self.normal = axu.getNormal(vel);
            
            //update the velocity based on the angle, unless it's already
            //been changed by the user, in which the angle/speed is updated
            if(!(ovel[0] === vel[0]&&ovel[1] === vel[1])){
                self.angle = angle = axu.getAngle(vel);
                self.speed = speed = Math.sqrt(Math.pow(vel[0],2)+Math.pow(vel[1],2))
            }
            vel[0] = speed*Math.cos(angle/180*Math.PI);
            vel[1] = speed*Math.sin(angle/180*Math.PI);
            ovel[0] = vel[0];
            ovel[1] = vel[1];            
            
            //cleaning up the angles
            if(angle > 360)self.angle = angle = angle-360;
            if(angle < 0)self.angle = angle = angle+360;
            
            moveout = false;
            if(poly){
                var npos = [pos[0]+vel[0],pos[1]+vel[1]];
                var xOut = false;
                var yOut = false;
                //check if the x position exits path
                if(!axu.inpoly(poly,npos[0],pos[1])){
                    moveout = true;
                    xOut = true;
                }else{
                    pos[0] = npos[0];
                }
                //check if the y position exits path
                if(!axu.inpoly(poly,pos[0],npos[1])){
                    moveout = true;
                    yOut = true;
                }else{
                    pos[1] = npos[1];
                }
                //if there's a moveout find more information about it
                if(moveout){
                    //make a long line segment based on velocity to better check the
                    //collision
                    var oldColPos = [pos[0]-vel[0]*(self.width/10),pos[1]-vel[1]*(self.height/10)];
                    var newColPos = [pos[0]+vel[0]*(self.width/10),pos[1]+vel[1]*(self.height/10)];
                    var result = axc.linePolyIntersect(oldColPos,newColPos,poly)
                    if(result){
                        if(self.onmoveout)self.onmoveout(result[0],[result[1],result[2]]);
                        //update coordinates that move out to edge based on which hits out
                        if(xOut){
                            pos[0] = result[0][0];
                        }
                        if(yOut){
                            pos[1] = result[0][1];
                        }
                    }
                }
            }else {
                //if there is no path poly then just update position
                pos[0]+=vel[0];
                pos[1]+=vel[1];
            }
            //update the old position
            opos[0] = pos[0];
            opos[1] = pos[1];
            
            //update collision position if needed
            if(collideRef !== null){
                axc.collisions[collideRef].pos=pos;
                axc.collisions[collideRef].speed=vel;
            }
            //check mouse and set up events
            axu.isMouseOver(self,self.fixed);
            
            //keep alpha in bounds and apply it
            if(self.alpha > 1.0)self.alpha = 1.0;
            if(self.alpha < 0.0)self.alpha = 0.0;

            //do moveto if it exists
            if(moveto){
                dis = axu.distance(pos,moveto[0]);
                if(moveout){
                    moveto = undefined;
                    moveout = false;
                    self.speed = 0;
                    if(self.onarrive){
                        self.onarrive();
                    }
                    return;
                }
                if(dis < 30){
                    moveto = undefined;
                    moveout = false;
                    self.speed = 0;
                    if(self.onarrive){
                        self.onarrive();
                    }
                    return;
                }
                
                if(!axu.isfacing(self, {pos:moveto[0]},10)){
                    self.angle = axu.direction(pos,moveto[0]);
                }
                if(moveto && moveto[1] === undefined){
                    self.vel[0] = (moveto[0][0] - self.pos[0])*0.1;
                    self.vel[1] = (moveto[0][1] - self.pos[1])*0.1;
                }
                if(dis < 30 && moveto && moveto[1]){
                    self.vel[0] = (moveto[0][0] - self.pos[0])*0.3;
                    self.vel[1] = (moveto[0][1] - self.pos[1])*0.3;
                }
                
                 if(moveto && moveto[1]){
                    self.speed = moveto[1];
                }

            }
            
        };
    })();
    
    self.debug = function(){
         //debug drawing
        if(ax.debug && self.onScreen){
            c = ax.ctx;
            vel = self.vel;
            pos = self.pos;
            //bounding box
            axd.rect(true,0,0,self.width,self.height,false,[1,0,0,250]);
            if(collideRef !== null)
                axd.cir(true,0,0,axc.collisions[collideRef].rad,false,[1,0,255,0]);
            //speed    
            var length = Math.sqrt(Math.pow(self.width, 2)+Math.pow(self.height, 2))/5;
            var ratx = self.width/10;
            var raty = self.height/10;
            c.save();
            c.strokeStyle = "rgb(0,250,0)";
            c.beginPath();
            c.moveTo(pos[0]+ax.scrollx,pos[1]+ax.scrolly);
            c.lineTo(pos[0]+vel[0]*ratx+ax.scrollx,pos[1]+vel[1]*raty+ax.scrolly);
            c.closePath();
            c.stroke();
            c.restore();
            //path poly
            if(self.path){
                ax.ctx.restore();
                axd.poly(self.path,0,0);
            }
        }
    };
    
    self.moveto = (function(){
        var dis,angle,cnt = 0;pos = self.pos;
        return function (xy,speed){
            if(!xy){
                moveto = undefined;
                if(self.onarrive){
                    self.onarrive();
                }
                return;
            }
            self.angle = axu.direction(pos,xy);
            xy[0] = Math.floor(xy[0]);
            xy[1] = Math.floor(xy[1]);
            moveto = [xy,speed];
            
        };
    })();
    
    self.changeRef = function(newRef){
        sceneRef = newRef;
    };
    
    self.kill = function(){
        if(self.layer === "menu"){ //set aside for menu items
            ax.menu[sceneRef] = null;
        } else {
            ax.playarea[self.layer][sceneRef] = null;
        }

        axc.collisions[collideRef] = null;
        animations = null;
        self.state = -1;
    };
    self.chgLayer = function (lyr){
        if(!(lyr >= 0)){ //must supply a layer to move it to
            if(lyr !== "menu"){
                throw "Must supply layer when changing layer, id:"+id;
            }
        }
        //kill off the old scenegraph reference
        if(self.layer === "menu"){ //set aside for menu items
            ax.menu[sceneRef] = null;
        } else {
            ax.playarea[self.layer][sceneRef] = null;
        }
        self.layer = lyr; //set the new layer value
        if(self.layer === "menu"){// add menu item
            sceneRef = ax.menu.push(self)-1;
        } else { //add the actor in the new location
            if(!ax.playarea[self.layer]){
                ax.playarea[self.layer] = [];
            }
            sceneRef = ax.playarea[self.layer].push(self)-1;
        }        
    };
   
    self.draw = function(){
        if(self.state === -2)return;
        
        //check if the object is on screen
        axu.isOnScreen(self,self.fixed);
        
        var c = ax.ctx;
        c.globalAlpha = self.alpha;
        
        if(!self.fixed){
            c.translate(Math.round(self.pos[0]+ax.scrollx),Math.round(self.pos[1]+ax.scrolly));
        } else {
            c.translate(Math.round(self.pos[0]),Math.round(self.pos[1]));
        }
        if(self.rot)c.rotate(self.rot);        
        
        if(self.aniName){
            if(!ani){
                self.changeAni(self.aniName);
                return;
            }
            if(self.currentFrame >= ani.length){
                if(ani.onended){
                    ani.onended(self);
                    self.currentFrame = ani.length-1;
                }

                if(self.state > 0)
                    self.currentFrame = 0;
            }
            axd.image(true,ani[self.currentFrame],0,0);
            if(self.state > 0)
                self.currentFrame++;
        }else if(self.stamp){
            axd.image(true,self.stamp,0,0);
        }
        //draw text if it exists
        if(self.text.txt !== ""){
            self.drawText();
        }
    };
    self.drawScript = function (){};
    self.gameScript = function (){};
    
    //if the collide ref is set on init
    if(self.collideRadius){
       if(self.fixed){
            collideRef = axc.addCollider(self,[self.pos[0]+ax.scrollx,self.pos[1]+ax.scrolly],self.collideRadius);
        }else {
            collideRef = axc.addCollider(self,self.pos,self.collideRadius);
        }
    }
    
    //add to layer array in scenegraph
    if(self.layer === "menu"){//set aside for menu items
        sceneRef = ax.menu.push(self)-1;
    } else {
        if(!ax.playarea[self.layer]){
            ax.playarea[self.layer] = [];
        }
        sceneRef = ax.playarea[self.layer].push(self)-1;
    }
    return self;
};

//draw everything to screen
ax.draw = function (){    
    if(!ax.windowActive){
        return;
    }
    
    axd.clr(); //clear the screen
    //draw the first layer of the level
    if(ax.level)ax.level.draw1();
    var c = ax.ctx; //stores the contex in a local var for speed
    //draw playarea actors and draw stuff
    for(var i = 0, l = ax.playarea.length; i<l; i++){
        if(!ax.playarea[i]){
            ax.playarea[i] = [];
            continue;
        }
        ax.playarea[i].map(
            function(act){
                if(act){
                    c.save(); //makes sure the canvas always returns to its state
                    act.draw();
                    act.drawScript();
                    act.debug();
                    c.restore();
                }
            }
        );
    }
    //draw the next layers in the level
    if(ax.level)ax.level.draw2();
    //execute the custom drawing code
    ax.drawLoop(); 
    //draw camera if needed
    ax.camera.draw();
    
    //drawing just the menu layer
    ax.menu.map(
        function(act){
            if(act){ //select only unkilled actors
                c.save(); //makes sure the canvas always returns to its state
                act.draw();
                act.drawScript();
                act.debug();
                c.restore();
            }
        }
    );
    ax.scriptDebugPos[1] = 10;
};
ax.updateLoop = function (){
    var olddate = (new Date).getTime();
    if(ax.windowActive){
        ax.update();
        ax.gameLoop();
        axe.clear();
        ax.camera.update();
        axc.checkCollision();
    }
    setTimeout(ax.updateLoop, 1000/ax.fps-((new Date).getTime() - olddate));
};


//The update command that goes through the layers and
//updates each actor
ax.update = function (){
    ax.everyActor(function(act){act.update();act.gameScript();});
    
    //remove killed actors
    ax.cleanActors();
};


ax.gameLoop = function (){
    //custom logic code goes here
};
ax.drawLoop = function (){
    //custom drawing code goes here
};

//scene director code
ax.direct = (function (){
    var scenes = {};
    
    return {
        saveScene : function (name, init){
            scenes[name]=init;
        },
        playScene : function (name){
            if(!scenes[name])throw "Scene doesn't exist";
            ax.camera.reset();
            axc.collisions = [];
            ax.gameLoop = function(){};
            ax.drawLoop = function(){};
            ax.everyActor(//kill all the existing actors
                function(actor){if(actor.kill)actor.kill();}              
            );
            ax.menu = [];
            ax.playarea = [];
            ax.level = undefined;
            scenes[name]();
        }
    };
})();

//scripted events and cmds
ax.scriptDebugPos =[10,10];
ax.killAllScripters = function (){
    ax.everyActor(function(act){
        if(act.id[0] === "S"){
            act.kill();
        }
    });
};
ax.Scripter = function(){
    var self = this;
    var timer = 0;
    var commands = [];
    var place = null;
    var pause = false;
        
    self.fire = function(){
        if(place){
            ax.playarea[place] = null;
            place = null;
            timer = 0;
        }
        if(!ax.playarea[0]){
            ax.playarea[0] = [];
        }
        place = ax.playarea[0].push(self)-1;
    };
    
    self.update = function(){
        if(pause)return;
        
        var l = commands.length;
        var i = 0;
        var cmd;
        while(i < l){
            cmd = commands[i++];
            if(cmd[0] === timer/ax.fps){
                cmd[1]();
                if(i === l){
                    if(!self.loop){
                        ax.playarea[0][place] = null;
                        place = null;
                        timer = 0;
                    } else {
                        timer = 0;
                    }
                }
            }
        }
        timer++;
    };
    self.changeRef = function(newRef){
        place = newRef;
    };
    self.cmd = function(time, func){
        commands.push([time,func]);
    };
    self.cmds = function(){
        var l = arguments.length;
        var i = 0;
        var obj;
        while(i < l){
            obj = arguments[i++];
            commands.push([obj[0],obj[1]]);
        }
    };
    self.kill = function (){
        ax.playarea[0][place] = null;
        place = null;
        timer = 0;
    }
    self.onOff = function(){
        pause = !pause;
    };
    //fillers
    self.show = function(){return [timer,commands]};
    self.id = "S"+axu.randBet(0,9999999999999999);
    self.debug = function(){
        if(ax.debug){
            ax.ctx.save();
            ax.ctx.font = "10pt Arial";
            ax.ctx.fillStyle = "rgb(0,200,0)";
            ax.ctx.fillText(
                            "[ "+self.id+"  <"+Math.round(timer/ax.fps)+":"+timer+"> ]",
                            ax.scriptDebugPos[0],
                            ax.scriptDebugPos[1]);  
            ax.ctx.restore();
            ax.scriptDebugPos[1] += 12;
        }
    };
    self.loop = false;
    self.draw = function(){};
    self.drawScript = function(){};
    self.gameScript = function(){};
};


ax.Init = function (fps, parent){
    //default settings
    fps = fps || 30; 
    
    BrowserDetect.init();
    
    //setting the right clear function
    //for firefox a simple clearRect is faster
    if(BrowserDetect.browser === "Firefox"){
        axd.clr = function (){
            var c = ax.ctx;
            //c.setTransform(1,0,0,1,0,0);
            c.clearRect(0,0,ax.width,ax.height);
        };
    }else {
        axd.clr = function () {
            ax.canvas.width = ax.canvas.width;
        };
    }
    
    ax.makeGame();
    //setting diminsions
    ax.setDiminsions();    
    //start input capturing
    axe.init(fps, parent);
    //start collision detection
    axc.init();
    //set window events
    window.addEventListener("blur",function(){
        ax.windowActive = false;
    });
    //have game pause when focus is away
    window.addEventListener("focus",function(){
        ax.windowActive = true;
    });
    
    //start loop
    ax.fps = fps;
    setTimeout(ax.updateLoop, 1000/ax.fps);
    setInterval(ax.draw, 1000/ax.fps);
};




