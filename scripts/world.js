//all the world info

var world  = {};
world.regions = {};

//makes the region object to attach locations to.
world.makeRegion = function (region){
    region.name = region.name || "test"+axu.randBet(0,9999);
    region.locations = [];
    region.overlays = [];
    //adds a city location
    region.addCity  = function(city){
        city.name = city.name || "test";
        city.image = city.image || "testCity";
        city.pop = city.pop || 1000;
        city.background = city.background || "cityImg1";
        city.restCost = city.restCost || 20;
        city.fans = city.fans || 0;
        city.economy = city.economy || "good";
        city.found = city.found || false;
        city.travelCost = city.travelCost || 30;
        city.type = "city";
        city.map = null;
        
        region.locations.push(city);
    };
    //add overlays to the region
    region.addOverlays = function(){
        region.overlays = [];
        
       for(var iterator = 0, length = arguments.length;
           iterator < length; iterator++){
            region.overlays.push(arguments[iterator]);
       }
    };
    //returns a location based on the name
    region.returnLocation = function(name){
        var key = 0;
        var list = region.locations;
        var length = list.length;
        var location = null;
        
        for(;key < length; key++){
            if(list[key].name === name){
                location = list[key];
                break;
            }
        }
        
        return location;
    };
    //make the overlays of the region
    region.makeOverlays = function (){
        var mkO = function (imageName, x, y){
           var o =  new ax.Actor({
                stamp: axr.images[imageName],
                width:47,
                height:27,
                layer:3,
                pos:[x,y],
                fixed:true
            });
           o.gameScript = function (){
                if(o.mhover){
                    o.alpha = 1.0;
                }else {
                    o.alpha = 0.4;
                }
            };
            return o;
        };
        region.overlays.map(function(overlay){
            mkO(overlay[0], overlay[1], overlay[2]);
        });        
    };
    //makes all the cities of the region
    region.makeCities = function(){
        var mkC = function(cityObj){
            
            var city = new ax.Actor({
                stamp: axr.images[cityObj.image],
                pos: cityObj.pos,
                type: "city",
                width: 20,
                height: 20,
                layer: 1
            });
            city.name = cityObj.name;
            city.text.width = 35;
            city.text.height = 10;
            city.text.size = 7;
            city.text.offset = [0,-15];
            city.text.color = "#ffffff";
            
            city.makeCollidable(10);
            
            var cityTxtOn = false;
            city.gameScript = function(){
                if(city.mhover){
                    if(!cityTxtOn){
                        city.text.txt = cityObj.name;
                        cityTxtOn = !cityTxtOn;
                    }
                }else {
                    if(cityTxtOn){
                        city.text.txt = "";
                        cityTxtOn = !cityTxtOn;
                    }
                }
            };
        };
        region.locations.map(mkC);
    };
    //make everything
    region.make = function(){
        mh.currentRegion = region.name;
        //make the game map
        var map = new ax.Actor({
            stamp: axr.images.world,
            width:700,
            height:500
        });
        region.map = map;
        //make the cities
        region.makeCities();
        region.makeOverlays();
    };
    
    world.regions[region.name] = region;
    return region;
};

world.makeRegion({
    name: "Durra",
    map: axr.images.world
});
//city information for Durra
world.regions["Durra"].addCity({
    name: "Thule",
    pop: 16777,
    pos: [30,180],
    store: true,
    quests: true
    
});
world.regions["Durra"].addCity({
    name: "Pom Tsu",
    pop: 4977,
    pos: [-150,130],
    store: true,
    quests: true
});
world.regions["Durra"].addCity({
    name: "Ys",
    pop: 1773,
    pos: [60,-50],
    store: true,
    quests: true,
    found: true
});
world.regions["Durra"].addCity({
    name: "Hitifi",
    pop: 407,
    pos: [70,-170],
    store: true,
    quests: true
});
world.regions["Durra"].addCity({
    name: "Petra",
    pop: 14525,
    pos: [-240,-200],
    store: true,
    quests: true
});
//overlays for Durra
world.regions["Durra"].addOverlays(
    ["creepForest", 520, 30],
    ["sunkSwamp", 470, 300],
    ["mirDesert", 260, 90],
    ["peekPlains", 230, 330],
    ["starLake", 350, 220],
    ["desineSea", 290, 480],
    ["crystalBeach", 350, 460]
);