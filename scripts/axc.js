var axc = {};

axc.init = function(){
	/*axc.canvas = document.createElement('canvas');
	axc.ctx = axc.canvas.getContext('2d');*/
	axc.collisions = [];
	
};

axc.addCollider = function(actor,pos,rad){//give uncentered position
	var obj = {
		pos:pos,
		speed:[],
		rad:rad,
		actor:actor,
		collList:[]
	};
	obj.isColliding = (function(i){
		
		return function(base){
			i = obj.collList.length;
			while(i--){
				if(obj.collList[i] === base){
					return true;
				}
			}
			return false;
		};
	})();
	obj.removeColl = (function(i,l,nArray){

		return function(base){
			l = obj.collList.length;
			i = 0;
			nArray = [];
			while(i<l){
				if(obj.collList[i] !== base){
					nArray.push(obj.collList[i]);
				}
				i++;
			}
			obj.collList = nArray;
		};
	})();
	return axc.collisions.push(obj)-1;
};

axc.checkCollision = (function(){
	var l, i, ni, dis;
	var ob1,ob2;
	
	return function(){
		l = axc.collisions.length;
		i = 0;

		while(i < l){
			ni=i+1;
			ob1 = axc.collisions[i++];
			if(!ob1)continue;
			
			while(ni < l){
				ob2 = axc.collisions[ni++];
				if(!ob2)continue;
                
				dis = Math.sqrt(Math.pow((ob1.pos[0]-ob2.pos[0]),2)+
								Math.pow((ob1.pos[1]-ob2.pos[1]),2));
								
				if(ob1.isColliding(ni)){
					if(ob1.rad+ob2.rad < dis){
						ob1.removeColl(ni);
					}else{
						if(ob1.actor.oncolliding){
							ob1.actor.oncolliding(ob2.actor,ob1.actor);
						}
						if(ob2.actor.oncolliding){
							ob2.actor.oncolliding(ob1.actor,ob2.actor);
						}
					}
				}else{
					if(ob1.rad+ob2.rad >= dis){
						ob1.collList.push(ni);
						if(ob1.actor.oncollide){
							ob1.actor.oncollide(ob2.actor,ob1.actor);
						}
						if(ob2.actor.oncollide){
							ob2.actor.oncollide(ob1.actor,ob2.actor);
						}

					}
				}
				

			}
			
		}
	};
		
})();

axc.linePolyIntersect = function (L1,L2,poly){
    var result = false
    for(var i = 0,l = poly.length,vert; i<l; i++){
		var point1 = poly[i];
		var point2 = poly[i+1];
	    if(!point2)point2 = poly[0];
        vert = axc.lineIntersect(L1,L2,point1,point2);
	
        if(vert){
            result =  [vert,point1, point2];
		    break;
        }
    }
    return result;
};
axc.lineIntersect = function(a1, a2, b1, b2) {
    var result = false;
    
    var ua_t = (b2[0] - b1[0]) * (a1[1] - b1[1]) - (b2[1] - b1[1]) * (a1[0] - b1[0]);
    var ub_t = (a2[0] - a1[0]) * (a1[1] - b1[1]) - (a2[1] - a1[1]) * (a1[0] - b1[0]);
    var u_b  = (b2[1] - b1[1]) * (a2[0] - a1[0]) - (b2[0] - b1[0]) * (a2[1] - a1[1]);

    if ( u_b != 0 ) {
        var ua = ua_t / u_b;
        var ub = ub_t / u_b;

        if ( 0 <= ua && ua <= 1 && 0 <= ub && ub <= 1 ) {
            //they intersect
            result = [a1[0] + ua * (a2[0] - a1[0]),a1[1] + ua * (a2[1] - a1[1])];
        } else {
            result = false;
        }
    } else {
        if ( ua_t == 0 || ub_t == 0 ) {
            result = false;
        } else {
            result = false;
        }
    }

    return result;
};
