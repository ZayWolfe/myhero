//screens.js
var mh = {};

/*
 * All the code for the  announcements panel. Internally it should
 * work just like the events panel but should pull announcement
 * data from a server and display it. 
*/
 mh.mAScreen = function (){
    //creating the actual actor
    var aScreen = new ax.Actor({
		type:"announce screen",
		stamp: axr.images.announce,
		pos: [55,213],
        layer:4,
		width: 100,
		height: 421,
		fixed:true
	});
    //creating an actor for the actual button so that it can be
    //indepedentally clicked from the panel
	aScreen.button = new ax.Actor({
		stamp: axr.images.button,
		width:16,
		height:16,
        layer:4,
		pos:[100-10,13],
		fixed:true
	});
    //setting the mouse click function
	aScreen.button.leftUp = (function(){
		var extended = true;
		return function(){
            axe.mouse.leftUp = false;
			if(extended){
				aScreen.stamp = axr.images.announceC;
				aScreen.pos[1]-=200;
				aScreen.height = 21;
			}
			else{
				aScreen.stamp = axr.images.announce;
				aScreen.pos[1]+=200;
				aScreen.height = 421;
			}
			extended = !extended;
		};
	})();
    
    return aScreen;
 };
 
 /*
* makes and sets up the events screen before returning it.
* Important part of the game, it shows in a list everything that's
* happening with the time it happens. Internally it shows everything
* inside the actor as it's text overlay. It keeps track of an array of
* events and makes sure to only ever draw so many of the newest
* events, deleting older events.
*/
 mh.mEScreen = function (){
    //makes the actor object for it
    var eScreen = new ax.Actor({
		type:"events screen",
		stamp: axr.images.events,
		pos: [ax.width-55,138],
		width: 100,
		height: 270,
        layer:4,
		fixed:true
	});
    //The events array and the addEvent function that adds an
    //event to the array and kicks off older events once a limit has
    //been reached.
    eScreen.events = [];
    eScreen.addEvent = function(msg){
        if(eScreen.events.length >= 20){
            eScreen.events.shift();
        }
        eScreen.events.push(msg);
    };
    //the text overlay properties
    eScreen.text.txt = " ";
    eScreen.text.color = "#ffffff";
    eScreen.text.size = 7;
    eScreen.text.height = 246;
    eScreen.text.width = 95;
    eScreen.text.offset = [0,10];
    eScreen.text.spacing = 9;
    //gamescript that always updating the event text in the actor
    eScreen.gameScript = function() {
        eScreen.text.txt = ""
        for(var i = 0,l = eScreen.events.length; l > i; l--){
            eScreen.text.txt += eScreen.events[l-1] + " ------------------------------ ";
            
        }
    };
	eScreen.button = new ax.Actor({
		stamp: axr.images.button,
		width:16,
		height:16,
        layer:4,
		pos:[ax.width-15,13],
		fixed:true
	});    
	eScreen.button.leftUp = (function(){
		var extended = true;
		return function(){
            axe.mouse.leftUp = false;
			if(extended){
				eScreen.stamp = axr.images.eventsC;
				eScreen.pos[1]-=125;
				eScreen.height = 21;
                eScreen.text.on = false;
			}
			else{
                eScreen.text.on = true;
				eScreen.stamp = axr.images.events;
				eScreen.pos[1]+= 125;
				eScreen.height = 214;
			}
			extended = !extended;
		};
	})();
    
    return eScreen;
 };
 
 // makes and sets up the character screen and then returns it
 mh.mCScreen = function (position){
    position = position || [ax.width-104,390];
    
    var cScreen = new ax.Actor({
		type:"char screen",
		stamp: axr.images["char"],
		pos: position,
		width: 198,
		height: 207,
        layer:4,
		fixed:true
	});
    //the open inventory button
    cScreen.button = new ax.Actor({
		stamp: axr.images.button2,
		width:16,
		height:16,
        layer:4,
		pos:[position[0]+89,position[1]-95],
		fixed:true
	});    
	cScreen.button.leftUp = (function(){
		var invOpen = false;
        var inventory = null;
		return function(){
            axe.mouse.leftUp = false;
			if(invOpen){
                inventory.killall();
                cScreen.button.stamp = axr.images.button2;
			} else {
                cScreen.button.stamp = axr.images.button;
                inventory = mh.mInventory()
			}
			invOpen = !invOpen;
		};
	})();
    
    var hpBar = mh.mBar(
        function(){return mh.hero.hp[0]/mh.hero.hp[1]},
        [position[0]-40, position[1]-97],
        [190,0,0],
        4,
        true
    );
    var enBar = mh.mBar(
        function(){return mh.hero.energy[0]/mh.hero.energy[1]},
        [position[0]-40, position[1]-89],
        [100,100,250],
        4,
        true
    );

    return cScreen;
 };
 
 mh.mBar = function(stat, pos, color, layer, fixed) {
    color = color || [250,0,0];
    
    var bar = new ax.Actor({
        width:94,
        height: 6,
        pos: pos,
        fixed: fixed,
        layer: layer,
        stamp: axr.images.bar
    });
    
    var full = 95, size = 94, offset = 0;
    
    bar.drawScript = function(){
        size = Math.round(full * stat());
        offset = full - size;
        axd.rect(true, -offset/2-1, 0, size, 6, color);
        
    };
    
    return bar;    
 };
 
 mh.mSaveDialog = function(){
    var dialog = new ax.Actor({
        type: "dialog",
        width: 200,
        height: 67,
        stamp:axr.images.confirm,
        pos: [ax.halfWidth, ax.halfHeight-100],
        fixed: true,
        layer: 4
    });
   var saveIcon= new ax.Actor({
        type: "icon",
        width: 28,
        height: 28,
        stamp:axr.images.saveIcon,
        pos: [ax.halfWidth-60, ax.halfHeight-110],
        fixed: true,
        alpha:0.7,
        layer: 4
    });
    saveIcon.text.width = 40;
    saveIcon.text.height = 15;
    saveIcon.text.size = 9;
    saveIcon.text.color = "#ffffff";
    saveIcon.text.txt = "Save";
    saveIcon.text.offset = [6,25];
    saveIcon.gameScript = function() {
        if(saveIcon.mhover){
            saveIcon.alpha = 1.0;
        }else {
            saveIcon.alpha = 0.7;
        }
    };
    saveIcon.leftUp = function() {
        axe.mouse.leftUp = false;
        mh.saveGameData();
        mh.makeEventMsg("Game Saved.");
        killall(); 
    };
   
   var clearIcon = new ax.Actor({
        type: "icon",
        width: 40,
        height: 28,
        stamp:axr.images.saveClear,
        pos: [ax.halfWidth+5, ax.halfHeight-107],
        fixed: true,
        layer: 4
    });
    clearIcon.text.width = 40;
    clearIcon.text.height = 15;
    clearIcon.text.size = 9;
    clearIcon.text.color = "#ffffff";
    clearIcon.text.txt = "Delete";
    clearIcon.text.offset = [-2,22];
    clearIcon.gameScript = function() {
        if(clearIcon.mhover){
            clearIcon.alpha = 1.0;
        }else {
            clearIcon.alpha = 0.7;
        }
    };
    clearIcon.leftUp = function() {
        axe.mouse.leftUp = false;
        if(confirm("Are you sure you wish to delete your saved game?")){
            sessionStorage.clear("myhero");
            ax.direct.playScene("loading");
        }
    };
   
   var cancelIcon = new ax.Actor({
        type: "icon",
        width: 28,
        height: 28,
        stamp:axr.images.cancelIcon,
        pos: [ax.halfWidth+60, ax.halfHeight-110],
        fixed: true,
        layer: 4
    });
    cancelIcon.text.width = 40;
    cancelIcon.text.height = 15;
    cancelIcon.text.size = 9;
    cancelIcon.text.color = "#ffffff";
    cancelIcon.text.txt = "Cancel";
    cancelIcon.text.offset = [6,25];
    cancelIcon.gameScript = function() {
        if(cancelIcon.mhover){
            cancelIcon.alpha = 1.0;
        }else {
            cancelIcon.alpha = 0.7;
        }
    };
    cancelIcon.leftUp = function() {
        axe.mouse.leftUp = false;
        killall(); 
    };
    
    var killall = function(){
        dialog.kill();
        saveIcon.kill();
        clearIcon.kill();
        cancelIcon.kill();
    };
    
 };
 
 mh.mInventory = function(){
    var category = "misc";
    var itemList;
    var updateInventory = function(){
        //cleaning out inventory
        mh.hero.inv = mh.hero.inv.filter(function(value){return value});
       //selecting the category
       itemList = mh.hero.inv.filter(function(value){
           return (value.category === category);
       });
    };
    updateInventory();
    
    var scroll = 0;
    
    var inv = new ax.Actor({
        type: "Inventory screen",
        stamp: axr.images.inventory,
        pos: [ax.halfWidth,ax.halfHeight],
        width: 198,
        height: 207,
        layer: 4,
        fixed: true
    });
    inv.leftUp = function (){
        //axe.mouse.leftUp = false;
    };
    inv.minimize = new ax.Actor({
		stamp: axr.images.button,
		width:16,
		height:16,
        layer:4,
		pos:[ax.halfWidth+90,ax.halfHeight-95],
		fixed:true
	});
    inv.minimize.leftUp = function(){
        axe.mouse.leftUp = false;
        mh.cScreen.button.leftUp();
    };
    inv.children = [];
    inv.killall = function(){
        inv.minimize.kill()
        for(act in inv.children){
            if(inv.children[act].kill)
                inv.children[act].kill();
        }
        inv.kill();
    };
    
    //make the categories
    //a helper function to streamline the category making process
    var mkCat = function(text, width, pos){
        var cat =  new ax.Actor({
            width: width,
            height: 20,
            layer:4,
            pos: pos,
            fixed: true
        });
        cat.text.txt = text;
        cat.text.color = "#ffffff";
        cat.text.weight = "bold";
        cat.text.size = 9;
        
        cat.gameScript = function(){
            if(cat.mhover || category === text.toLowerCase()){
                cat.alpha = 1.0;
            }else {
                cat.alpha = 0.5;
            }
        };
        
        cat.leftUp = function(){
            axe.mouse.leftUp = false;
            category = text.toLowerCase();
            updateInventory();
        };
        
        inv.children.push(cat);        
        return cat;
    };
    var catMisc = mkCat("Misc", 30, [ax.halfWidth-75,ax.halfHeight-92]);
    var catWeapons = mkCat("Weapons", 50, [ax.halfWidth-30,ax.halfHeight-92]);
    var catArmor = mkCat("Armor", 40, [ax.halfWidth+20,ax.halfHeight-92]);
    var catKey = mkCat("Key", 30, [ax.halfWidth+60,ax.halfHeight-92]);

    //make the inventory actors
    var mkList = (function(){
        var place = 0;
        
        return function(){
            var list = new ax.Actor({
                stamp: axr.images.list,
                width: 175,
                height:30,
                pos:[ax.halfWidth-2,ax.halfHeight-65+35*place],
                layer:4,
                fixed: true
            })
            list.text.txt = "";
            list.text.color = "#ffffff";
            list.text.size = 9;
            list.text.height = 28;
            list.text.width = 173
            list.item = null;
            place++;
            
            list.gameScript = function(){
                var item = list.item;
                var label = "";
                if(list.item){
                    label = item.name
                    if(item.count > 1)//add on count if needed
                        label += " (" + item.count + ")";
                    if(item.equiped){
                        label += "  <E>";
                        if(list.stamp !== axr.images["list-e"])
                            list.stamp = axr.images["list-e"]
                    }else {
                        if(list.stamp !== axr.images["list"])
                            list.stamp = axr.images["list"]
                    }
                    
                }else {
                    if(list.stamp !== axr.images["list"])
                            list.stamp = axr.images["list"]
                }
                list.text.txt = label;
            };
            list.leftUp = function(){
                axe.mouse.leftUp = false;
                mh.useItem(list.item);
                updateInventory();
            };
            
            inv.children.push(list);
            return list;
        }
    })();
    var invList = [
                    mkList(),
                    mkList(),
                    mkList(),
                    mkList(),
                    mkList()
                   ];
    inv.invList = invList; //making it check-up-able
    
    //the gamescript that populates the list
    inv.gameScript = function(){
        var i = scroll;//need to make based on ratio
        var itemLength = itemList.length;
        var invPlace = 0;
        
        invList.map(function(actor){//clearing the items for update
            actor.item = null;
        });
        
        while( invPlace < 5 && i < itemLength){
            invList[invPlace].item = itemList[i];
            
            i++;
            invPlace++;
        }
        
        
    };

    return inv;
 };
 //selling inventory window. Pretty much the same as the
 //standard inventory without the minimize button and
 //different labels and it doesn't use items but removes them
 //and gives you the gold
 mh.mSellWindow = function(){
    var category = "misc";
    var itemList;
    var updateInventory = function(){
        //cleaning out inventory
        mh.hero.inv = mh.hero.inv.filter(function(value){return value});
       //selecting the category
       itemList = mh.hero.inv.filter(function(value){
           return (value.category === category);
       });
    };
    updateInventory();
    
    var scroll = 0;
    
    var inv = new ax.Actor({
        type: "Inventory screen",
        stamp: axr.images.inventory,
        pos: [ax.halfWidth+50,ax.halfHeight],
        width: 198,
        height: 207,
        layer: 4,
        fixed: true
    });
    inv.leftUp = function (){
        //axe.mouse.leftUp = false;
    };

    inv.children = [];
    inv.killall = function(){
        for(act in inv.children){
            if(inv.children[act].kill)
                inv.children[act].kill();
        }
        inv.kill();
    };
    
    //make the categories
    //a helper function to streamline the category making process
    var mkCat = function(text, width, pos){
        var cat =  new ax.Actor({
            width: width,
            height: 20,
            layer:4,
            pos: pos,
            fixed: true
        });
        cat.text.txt = text;
        cat.text.color = "#ffffff";
        cat.text.weight = "bold";
        cat.text.size = 9;
        
        cat.gameScript = function(){
            if(cat.mhover || category === text.toLowerCase()){
                cat.alpha = 1.0;
            }else {
                cat.alpha = 0.5;
            }
        };
        
        cat.leftUp = function(){
            axe.mouse.leftUp = false;
            category = text.toLowerCase();
            updateInventory();
        };
        
        inv.children.push(cat);        
        return cat;
    };
    var catMisc = mkCat("Misc", 30, [ax.halfWidth-25,ax.halfHeight-92]);
    var catWeapons = mkCat("Weapons", 50, [ax.halfWidth+20,ax.halfHeight-92]);
    var catArmor = mkCat("Armor", 40, [ax.halfWidth+70,ax.halfHeight-92]);
    var catKey = mkCat("Key", 30, [ax.halfWidth+110,ax.halfHeight-92]);

    //make the inventory actors
    var mkList = (function(){
        var place = 0;
        
        return function(){
            var list = new ax.Actor({
                stamp: axr.images.list,
                width: 175,
                height:30,
                pos:[ax.halfWidth+48,ax.halfHeight-65+35*place],
                layer:4,
                fixed: true
            })
            list.text.txt = "";
            list.text.color = "#ffffff";
            list.text.size = 9;
            list.text.height = 28;
            list.text.width = 173
            list.item = null;
            place++;
            
            list.gameScript = function(){
                var item = list.item;
                var label = "";
                if(list.item){
                    label = item.name
                    if(item.count > 1)//add on count if needed
                        label += " (" + item.count + ")";
                    if(item.equiped){
                        label += "  <E>";
                        if(list.stamp !== axr.images["list-e"])
                            list.stamp = axr.images["list-e"]
                    }else {
                        if(list.stamp !== axr.images["list"])
                            list.stamp = axr.images["list"]
                    }
                    //write how much gold it is
                    label += "   "+item.cost+"g";
                    
                }else {
                    if(list.stamp !== axr.images["list"])
                            list.stamp = axr.images["list"]
                }
                list.text.txt = label;
            };
            list.leftUp = function(){
                axe.mouse.leftUp = false;
                //make sure you can't sell equiped items
                if(!list.item.equiped){
                    if(confirm("Sell " + list.item.name+" for "+list.item.cost+" gold?")){
                        mh.removeItemFromInventory(list.item);
                        mh.hero.gold += list.item.cost;
                    }
                }
                updateInventory();
            };
            
            inv.children.push(list);
            return list;
        }
    })();
    var invList = [
                    mkList(),
                    mkList(),
                    mkList(),
                    mkList(),
                    mkList()
                   ];
    inv.invList = invList; //making it check-up-able
    
    //the gamescript that populates the list
    inv.gameScript = function(){
        var i = scroll;//need to make based on ratio
        var itemLength = itemList.length;
        var invPlace = 0;
        
        invList.map(function(actor){//clearing the items for update
            actor.item = null;
        });
        
        while( invPlace < 5 && i < itemLength){
            invList[invPlace].item = itemList[i];
            
            i++;
            invPlace++;
        }
        
        
    };
    
    return inv;
 };


mh.mCityCraft = function(){
    var window = new ax.Actor({
        type: "window",
        width: 500,
        height:400,
        pos:[-50,20],
        layer:2,
        stamp: axr.images.cityWindow
    });
    
    window.killall = function(){
        window.kill();
    }
    
    return window;
};
mh.mCityShop = function(){
    var window = new ax.Actor({
        type: "window",
        width: 500,
        height:400,
        pos:[-50,20],
        layer:2,
        stamp: axr.images.cityWindow
    });
    window.children = [];
    
    window.killall = function(){
        window.kill();
        sellLabel.kill()
        sellWindow.killall();
        goldCount.kill();
        goldCountScpt.kill();
    }
    
    //labels
    var sellLabel = new ax.Actor({
        type: "label",
        width: 50,
        height: 25,
        pos: [60,-130],
        layer:3
    })
    sellLabel.text.txt = "Sell";
    sellLabel.text.color = "#ffffff";
    sellLabel.text.size = 18;
    sellLabel.text.weight = "bold";
    
    var goldCount = new ax.Actor({
        type: "label",
        width: 260,
        height: 25,
        pos: [60,180],
        layer:3
    })
    var goldCountScpt = new ax.Scripter();
    goldCountScpt.cmd(2,function(){
        goldCount.text.txt = "Gold: "+mh.hero.gold;
        });
    goldCountScpt.loop = true;
    goldCountScpt.fire();
    goldCount.text.txt = "Gold: "+mh.hero.gold;
    goldCount.text.color = "#eeb422";
    goldCount.text.size = 18;
    goldCount.text.weight = "bold";
    
    var sellWindow = mh.mSellWindow();
    
    return window;
};
